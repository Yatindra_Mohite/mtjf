<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
			
		/*if($this->check_authentication() != 'success')
        die;*/
	}

	function signup()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='' && $json_array->user_password!='')
	    	{
	    		$code = $json_array->code;
	    		$seleuser = $this->common_model->common_getRow('qalame_user',array('user_email'=>$json_array->user_email));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->user_status==1)
		    			{	
							$final_output['status'] = 'failed';
	    					$final_output['message'] = constant("signup_".$json_array->language."_1");
		    			}else
		    			{
		    				if($code!='')
		    				{
		    					$otp = '1234';
		    					$this->common_model->updateData("qalame_user",array('v_code'=>$otp),array('user_id'=>$seleuser->user_id));
		    				}
		    				$final_output['status'] = 'unverified_user';
	    					$final_output['message'] = constant("login_msg_".$json_array->language."_3");
		    			}
		    		}else
		    		{
		    			$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("admin_status_".$json_array->language."");
		    		}
	    		}else
	    		{
    				if($code==''){
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
    				}else
    				{
    					$v_Code = '1234';
    				}
	    			
	    			$insert = $this->common_model->common_insert("qalame_user",array('user_name'=>$json_array->user_name,'mobile_code'=>$code,'user_area_interest'=>$json_array->user_area_interest,'v_code'=>$v_Code,'user_email'=>$json_array->user_email,'user_password'=>md5($json_array->user_password),'create_date'=>date('Y-m-d H:i:s')));
	    			if(!empty($insert) && $insert != false)
	    			{
						if($json_array->user_email != 'qalameapplication@gmail.com')
	    				{
	    					$selemail = $this->db->select('user_id')->get_where("qalame_user",array('user_email'=>'qalameapplication@gmail.com'))->row();
	    					if(!empty($selemail))
	    					{
	    						//follow admin account by user
	    						$inserttt = $this->common_model->common_insert("qalame_follow_user",array('follower_id'=>$insert,'following_id'=>$selemail->user_id,'request_status'=>1,'create_date'=>date('Y-m-d H:i:s')));
	    					}
	    				}
	    				//Send verification mail
	    				if($code=='')
	    				{
							$url = "<a href=".base_url()."api/email_verification/".$this->common_model->encryptor_ym('encrypt',$insert)."/".$v_Code." target='_blank' title='' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
		    				$data = array('email'=>$json_array->user_email,
				                          'name'=>$json_array->user_name,
				                           'url'=>$url
				                          );
		    				$subject = 'Qalame App: Verification Link';
	                        $message = $this->load->view('email_template/template_verify.php',$data,TRUE);
					    	$email_from = 'no-reply@qalame.com';
					      	$email = $json_array->user_email;
					      	$headers  = 'MIME-Version: 1.0' . "\r\n";
					        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					        $headers .= 'From: '.$email_from. '\r\n';            // Mail it
					   		@mail($email, $subject,$message,$headers);

                    	}else {
		           			$msg = "Your OTP for Qalame is: ".$v_Code;
                    		
                    		//Send OTP
                    	}   
                       	$final_output['status'] = 'success';
	    				$final_output['message'] = constant("signup_".$json_array->language."_2");
	    				$final_output['data'] = $insert; 
	    			}else
	    			{
	    				$final_output['status'] = 'failed'; 
					 	$final_output['message'] = constant("some_error_".$json_array->language."");
	    			}
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("login_msg_".$json_array->language."_6");
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	function loginwith_otp()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	$mobileno = $json_array->mobile;
	    	$otpcode = $json_array->otp;
	    	$data['user_device_id'] = $json_array->user_device_id;
	    	$data['user_device_type'] = $json_array->user_device_type;
	    	$data['user_device_token'] = $json_array->user_device_token;
	    	$language = $json_array->language;
	    	if(!empty($otpcode) && strlen($otpcode)==4)
	    	{
	    		$checkotp = $this->common_model->common_getRow("qalame_user",array("user_email"=>$mobileno,"admin_status"=>1));
	    		if(!empty($checkotp))
	    		{	
	    			if($checkotp->v_code == $otpcode)
	    			{	
	    				$data['v_code'] = '';
	    				$data['user_status'] = 1;
	    				$data['update_date'] = date('Y-m-d H:i:s');
	    				$token = bin2hex(openssl_random_pseudo_bytes(16));
        				$token = $token.militime;
	    				$data['user_token'] = $token;
	    				$update = $this->common_model->updateData("qalame_user",$data,array('user_id'=>$checkotp->user_id));
	    				if($update==TRUE)
	    				{	
	    					$updatedevicetoken = $this->common_model->updateData('qalame_user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$checkotp->user_id,'user_device_id'=>$json_array->user_device_id));
	    						
	    					$image = '';
    						if(!empty($checkotp->user_image))
		    				{
		    					$image = base_url().'uploads/user_image/'.$checkotp->user_image;
		    				}
			    				$this->db->where('user_id', $checkotp->user_id);
	    						$this->db->where('post_status', 1);
								$postcount = $this->db->count_all_results('qalame_user_post');	//post count
			    				
			    				$this->db->where('following_id', $checkotp->user_id);
								$this->db->where('request_status', 1);
								$follower_count = $this->db->count_all_results('qalame_follow_user');	//follower count
			    				
			    				$this->db->where('follower_id', $checkotp->user_id);
								$this->db->where('request_status', 1);
								$following_count = $this->db->count_all_results('qalame_follow_user');	//following count

								$object = array(
									'user_id'=>$checkotp->user_id,
									'user_name'=>$checkotp->user_name,
									'user_image'=>$image,
									'mobile_code'=>$checkotp->mobile_code,
									'user_email'=>$checkotp->user_email,
									'user_dob'=>$checkotp->user_dob,
									'user_gender'=>$checkotp->user_gender,
									'user_location'=>$checkotp->user_location,
									'user_area_interest'=>$checkotp->user_area_interest,
									'user_device_type'=>$json_array->user_device_type,
									'user_device_id'=>$json_array->user_device_id,
									'user_device_token'=>$json_array->user_device_token,
									'user_token'=>$token,
									'post_count'=>$postcount,
									'follower_count'=>$follower_count,
									'following_count'=>$following_count,
									'account_mode'=>$checkotp->account_mode
									);
								$final_output['status'] = 'success';
				    			$final_output['message'] = constant("mobile_verifiy_".$language."_1");
								$final_output['data'] = $object;
	    				}else
	    				{
	    					$final_output['status'] = 'failed';
		    				$final_output['message'] = constant("some_error_".$json_array->language."");
	    				}
	    			}else
	    			{
	    				$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("mobile_verifiy_".$language."_2");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("admin_status_".$language."");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = "Invalid Request Parameter.";		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end otp verification (Y)

	function Resend_otp()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->mobile) && !empty($json_array->language))
	    {
	    	$checkotp = $this->common_model->common_getRow("qalame_user",array('user_email'=>$json_array->mobile,'admin_status'=>1));
	    	if(!empty($checkotp))
	    	{	
					//$otp = $this->common_model->randomuniqueCode();

	    			//type = 1 (v_code) && type =2 (pass_code)
		            $otp = '1234';
		           	$msg = "Your OTP for Qalame is: ".$otp;

		           	if($json_array->type == 1)
		           	{
		           		$updateotp = $this->common_model->updateData("qalame_user",array('v_code'=>$otp,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$checkotp->user_id));
		           	}
		           	else 
		           	{
		           		$updateotp = $this->common_model->updateData("qalame_user",array('pass_code'=>$otp,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$checkotp->user_id));
		           	}	
					
					if($updateotp!=false)
					{
						//Send OTP function
						$final_output['status'] = 'success';
						$final_output['message'] = constant("mobile_otp_".$json_array->language."_1");
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("some_error_".$json_array->language."");
					}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$json_array->language."");	
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end resend otp 17-11-17

	//
	function Resend_otp_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
		    $final_output = array();
	    	if(!empty($json_array->mobile) && !empty($json_array->code) && !empty($json_array->language))
	    	{   
	    		if($aa['data']->admin_status ==1){

	    			$userid = $aa['data']->user_id;
					//$otp = $this->common_model->randomuniqueCode();
		            $otp = '1234';
		           	$msg = "Your OTP for Qalame is: ".$otp;
		           
		           	$updateotp = $this->common_model->updateData("qalame_user",array('v_code'=>$otp,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$userid));
					
					if($updateotp!=false)
					{
						//Send OTP function
						$final_output['status'] = 'success';
						$final_output['message'] = constant("mobile_otp_".$json_array->language."_1");
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("some_error_".$json_array->language."");
					}
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
	    	}else
	    	{
		    	$final_output['status'] = 'failed';
		    	$final_output['message'] = "No Request Parameter Found.";
	    	}
	   }else
	    {
	    	$final_output = $aa;
	    }	

	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

	function login()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='' && $json_array->user_password!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('qalame_user',array('user_email'=>$json_array->user_email));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->user_status==1)
		    			{	
		    				if($seleuser->user_password == md5($json_array->user_password))
			    			{
	    						$token = bin2hex(openssl_random_pseudo_bytes(16));
        						$token = $token.militime;
	    						$update = $this->common_model->updateData('qalame_user',array('user_device_type'=>$json_array->user_device_type,'user_device_id'=>$json_array->user_device_id,'user_device_token'=>$json_array->user_device_token,'user_token'=>$token,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));
	    						
	    						$updatedevicetoken = $this->common_model->updateData('qalame_user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$seleuser->user_id,'user_device_id'=>$json_array->user_device_id));
	    						
	    						$seleuserdata = $this->common_model->common_getRow('qalame_user',array('user_email'=>$json_array->user_email));
	    						$image = '';
	    						if(!empty($seleuserdata->user_image))
			    				{
			    					$image = base_url().'uploads/user_image/'.$seleuserdata->user_image;
			    				}
			    				$this->db->where('user_id', $seleuserdata->user_id);
	    						$this->db->where('post_status', 1);
								$postcount = $this->db->count_all_results('qalame_user_post');	//post count
			    				
			    				$this->db->where('following_id', $seleuserdata->user_id);
								$this->db->where('request_status', 1);
								$follower_count = $this->db->count_all_results('qalame_follow_user');	//follower count
			    				
			    				$this->db->where('follower_id', $seleuserdata->user_id);
								$this->db->where('request_status', 1);
								$following_count = $this->db->count_all_results('qalame_follow_user');	//following count

								$object = array(
									'user_id'=>$seleuserdata->user_id,
									'user_name'=>$seleuserdata->user_name,
									'user_image'=>$image,
									'user_email'=>$seleuserdata->user_email,
									'mobile_code'=>$seleuserdata->mobile_code,
									'user_dob'=>$seleuserdata->user_dob,
									'user_gender'=>$seleuserdata->user_gender,
									'user_location'=>$seleuserdata->user_location,
									'user_area_interest'=>$seleuserdata->user_area_interest,
									'user_device_type'=>$seleuserdata->user_device_type,
									'user_device_id'=>$seleuserdata->user_device_id,
									'user_device_token'=>$seleuserdata->user_device_token,
									'user_token'=>$token,
									'post_count'=>$postcount,
									'follower_count'=>$follower_count,
									'following_count'=>$following_count,
									'account_mode'=>$seleuserdata->account_mode
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = constant("login_msg_".$json_array->language."_1");
	    						$final_output['data'] = $object;

			    			}else
			    			{
			    				//credentials does not matched
			    				$msg = "login_msg_".$json_array->language."_2"; 
			    				$final_output['status'] = 'failed';
	    						$final_output['message'] = constant($msg);
	    					}
		    			}else
		    			{
		    				if($seleuser->mobile_code!='')
		    				{
		    					$otp = '1234';
		    					$this->common_model->updateData("qalame_user",array('v_code'=>$otp),array('user_id'=>$seleuser->user_id));
		    				}

		    				$final_output['status'] = 'unverified_user';
	    					$final_output['message'] = constant("login_msg_".$json_array->language."_3");
		    			}
	    			}else
	    			{
						$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("admin_status_".$json_array->language."");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("login_msg_".$json_array->language."_5");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("login_msg_".$json_array->language."_6");
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end login (Y)
	function update_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$language = $this->input->post('language');
			if($aa['data']->admin_status==1)
			{
				$data['user_name'] = $this->input->post('user_name');
				$data['user_location'] = $this->input->post('user_location');
				$data['user_area_interest'] = $this->input->post('user_area_interest');
				$data['user_dob'] = $this->input->post('user_dob');
				$data['user_gender'] = $this->input->post('user_gender');
				if(!empty($_FILES['user_image']['name']) && isset($_FILES['user_image']))
				{
					$config = array();
					$config['upload_path']   = './uploads/user_image/';
					$config['allowed_types'] = 'jpg|jpeg|png';

					$subFileName = explode('.',$_FILES['user_image']['name']);
					$ExtFileName = end($subFileName);
				    $config['file_name'] = md5(militime.$_FILES['user_image']['name']).'.'.$ExtFileName;

					$this->load->library('upload',$config);
			        $this->upload->initialize($config);
					if($this->upload->do_upload('user_image'))
					{ 
						$image_data = $this->upload->data();
						$data['user_image'] = $image_data['file_name'];
						$image = base_url().'uploads/user_image/'.$image_data['file_name'];
						
					}
				}else
				{
					$getdata = $this->common_model->common_getRow("qalame_user",array('user_id'=>$aa['data']->user_id));	
					if($getdata->user_image!=''){
					$image = base_url().'uploads/user_image/'.$getdata->user_image;
					}else{
					 $image = '';
					}
				}
				$data['update_date'] = date('Y-m-d H:i:s');
				$update_data = $this->common_model->updateData("qalame_user",$data,array('user_id'=>$aa['data']->user_id));
				if($this->db->affected_rows())
				{
					$final_output['status'] = 'success'; 
				 	$final_output['message'] = constant("update_profile_".$language."_2");
					$final_output['data'] = array('user_image'=>$image); 
				}else
				{
					$final_output['status'] = 'failed'; 
				 	$final_output['message'] = constant("some_error_".$language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$language."");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end update profile (Y)
	function resend_verification_link()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
    		$seleuser = $this->common_model->common_getRow('qalame_user',array('user_email'=>$json_array->user_email));
    		if(!empty($seleuser))
    		{
    			if($seleuser->admin_status==1)
    			{
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
    				$update = $this->common_model->updateData("qalame_user",array('v_code'=>$v_Code),array('user_email'=>$json_array->user_email));
    				$subject = 'Qalame App: Verification Link';
                	$url = "<a href=".base_url()."api/email_verification/".$this->common_model->encryptor_ym('encrypt',$seleuser->user_id)."/".$v_Code." target='_blank' title='' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
    				$data = array('email'=>$json_array->user_email,
		                          'name'=>$seleuser->user_name,
		                           'url'=>$url
		                          );
                    $message = $this->load->view('email_template/template_verify.php',$data,TRUE);
    				$email_from = 'no-reply@qalame.com';
	              	$email = $json_array->user_email;
	              	$headers  = 'MIME-Version: 1.0' . "\r\n";
	                $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
	                $headers .= 'From: '.$email_from. '\r\n';            // Mail it
	           		@mail($email, $subject,$message,$headers);			

    				$final_output['status'] = 'success';
    				$final_output['message'] = constant("resent_".$json_array->language."_1");
    			}else
	    		{
	    			$final_output['status'] = 'failed';
    				$final_output['message'] = constant("admin_status_".$json_array->language."");
	    		}
			}else
    		{
    			$final_output['status'] = 'failed';
    			$final_output['message'] = constant("login_msg_".$json_array->language."_5");
	  		}
		}else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }		
		header("content-type: application/json");
	    echo json_encode($final_output);	
	}
	//end resend verificatin mail (Y)
	function email_verification()
	{
		$id = $this->uri->segment(3);
		$code = $this->uri->segment(4);
		$id =$this->common_model->encryptor_ym('decrypt',$id);
		
		$select = $this->db->get_where("qalame_user",array('user_id'=>$id))->row();
		if(!empty($select))
		{
				$data = array('email'=>$select->user_email
                             );
          	if($select->v_code == $code && !empty($select->v_code))
			{
				$this->db->cache_delete('user', 'verified'); //delete cache file

				$update = $this->db->update("qalame_user",array('v_code'=>'','user_status'=>1),array('user_id'=>$id));	
				
				$message = $this->load->view('email_template/successfully_verify.php',$data,TRUE);
				print_r($message);exit;
			}else
			{
				$message = $this->load->view('email_template/verification_failed.php',$data,TRUE);
				print_r($message);exit;
			}
		}else
		{
			$message = $this->load->view('email_template/verification_failed.php','',TRUE);
			print_r($message);exit;
		}
	}
	//end email verification (Y)
	function forgot_password()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
		$type = 1; //email

	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('qalame_user',array('user_email'=>$json_array->user_email));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if (!filter_var($json_array->user_email, FILTER_VALIDATE_EMAIL)) {
						  $type = 2; //mobile
						}
		    			if($seleuser->user_status==1)
		    			{
	    					if($type==1)
	    					{
		    					$c = $this->common_model->encryptor_ym('encrypt',$seleuser->user_id);
		    					$url = "<a href=".base_url()."change_password/account/".$c." target='_blank' title='' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
		    					$data = array('email'=>$seleuser->user_email,
	                         			'url'=>$url,
	                         			'name'=>$seleuser->user_name		
	                         				);
			    				$subject = 'Qalame App: Reset Password Link';
								$message = $this->load->view('email_template/template_forgot_pass.php',$data,TRUE);
			    				//$subject = 'Qalame App: Reset Password Link';
			                	//$message="For Reset Password: "."<a href=''>CLICK HERE</a>";
			    				$email_from = 'no-reply@qalame.com';
				              	$email = $json_array->user_email;
				              	$headers  = 'MIME-Version: 1.0' . "\r\n";
				                $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
				                $headers .= 'From: '.$email_from. '\r\n';            // Mail it
				           		@mail($email, $subject,$message,$headers);	
				           		$update = $this->common_model->updateData("qalame_user",array("pass_code"=>substr($c,6)),array('user_id'=>$seleuser->user_id));
	    					}else
	    					{
	    						$otp = '1234';
		           				$msg = "Your OTP for change password is: ".$otp;
	    						//Send OTP
				           		$update = $this->common_model->updateData("qalame_user",array("pass_code"=>$otp),array('user_id'=>$seleuser->user_id));
	    					}
			           		$final_output['status'] = 'success';
    						$final_output['message'] = constant("forgot_".$json_array->language."_1");
		    			}else
		    			{
		    				if($type==2)
	    					{
	    						$otp = '1234';
		           				$msg = "Your OTP is: ".$otp;
	    						
				           		$update = $this->common_model->updateData("qalame_user",array("v_code"=>$otp),array('user_id'=>$seleuser->user_id));
	    					}
		    				$final_output['status'] = 'unverified_user';
	    					$final_output['message'] = constant("login_msg_".$json_array->language."_3");
		    			}
	    			}else
	    			{
						$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("admin_status_".$json_array->language."");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("login_msg_".$json_array->language."_5");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);	
	}
	//end forgot password (Y)

	function send_password_mobile()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	$mobileno = $json_array->mobile;
	    	$otpcode = $json_array->otp;
	    	$password = $json_array->password;
	    	$language = $json_array->language;
	    	if(!empty($otpcode) && strlen($otpcode)==4)
	    	{ 
	    		$checkotp = $this->common_model->common_getRow("qalame_user",array("user_email"=>$mobileno,"admin_status"=>1));

	    		if(!empty($checkotp))
	    		{	
	    			if($checkotp->pass_code == $otpcode)
	    			{
		    			$data['pass_code'] = '';
	    				$data['user_password'] = md5($password);
	    				$data['update_date'] = date('Y-m-d H:i:s');
	    				$update = $this->common_model->updateData("qalame_user",$data,array('user_id'=>$checkotp->user_id));
	    				if($update==TRUE)
	    				{	
	    					$final_output['status'] = 'success';
			    			$final_output['message'] = constant("send_password_".$language."_1");
							//$final_output['data'] = $object;
	    				}else
	    				{
	    					$final_output['status'] = 'failed';
		    				$final_output['message'] = constant("some_error_".$json_array->language."");
	    				}
	    			}else
	    			{
	    				$final_output['status'] = 'failed';
	    				$final_output['message'] = constant("mobile_verifiy_".$language."_2");
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = constant("admin_status_".$language."");
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = "Invalid Request Parameter.";		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = "No Request Parameter Found.";
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end send_password_mobile (Y)

	function change_password()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array->language))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    if(!empty($json_array->old_password) && !empty($json_array->new_password))
				    {
				    	$selepas = $this->db->select('user_password')->get_where("qalame_user",array('user_id'=>$aa['data']->user_id,'user_password'=>md5($json_array->old_password)))->row();
				    	if(!empty($selepas))
				    	{
					    	$updatepass= $this->common_model->updateData("qalame_user",array('user_password'=>md5($json_array->new_password),'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$aa['data']->user_id));
					    	if($updatepass==true)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = constant("changed_".$json_array->language."_1");
					    	}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = constant("some_error_".$json_array->language."");
					    	}
				    	}else
				    	{
				    		$final_output['status'] = 'failed';
							$final_output['message'] = constant("changed_".$json_array->language."_3");
				    	}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("changed_".$json_array->language."_2");
				    }
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Chagne Password (Y)
	function area_of_interest_location_list()
	{
		$final_output = array();
		$arr = $locarr = array();
		$this->db->order_by('name','ASC');
		$getlist = $this->db->get_where("area_of_interest",array('admin_status'=>1))->result();
		if(!empty($getlist))
		{
			foreach ($getlist as $key) {
				
				$arr[] = $key;
			}
		}
		$this->db->order_by('name','ASC');
		$getlist11 = $this->db->get("location_list")->result();
		if(!empty($getlist11))
		{
			foreach ($getlist11 as $key) {
				
				$locarr[] = $key;
			}
		}
		$final_output['status'] = 'success';
    	$final_output['data'] = array('interest_area'=>$arr,'location'=>$locarr);
		header("content-type: application/json");
	    echo json_encode($final_output);	
	}
	//end area of interest and location list (Y)
	function check_version()
	{
		$json = file_get_contents('php://input');
		$json_array = json_decode($json);
		if($json_array->type=='android')
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>1))->row();
		}else
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>2))->row();
		}
		if(!empty($check_version))
		{
			$final_output['status'] = 'success';
	    	$final_output['message'] = 'successfully';
		}else
		{
			$final_output['status'] = 'failed';
	    	$final_output['message'] = constant("version_".$json_array->language."_1");
		}
		header("content-type: application/json");
	    echo json_encode($final_output);	
	}
	//end check app version (Y)	

	function follow_unfollow()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); //type 1=follow, 2=unfollow
			if(!empty($json_array->language) && !empty($json_array->following_id))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				   	if($json_array->following_id != $aa['data']->user_id)
				   	{
					 	$api_type = $json_array->api_type;
					   	$checkusr = $this->db->select('user_id,account_mode')->get_where("qalame_user",array('user_id'=>$json_array->following_id,'admin_status'=>1))->row();
						if(!empty($checkusr))
						{
					    	$status = 2;
					    	$selefollow = $this->db->select('follow_id')->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$json_array->following_id))->row();
					    	if($json_array->type==1)
					    	{
					    		if(empty($selefollow))
					    		{
					    			$selectname = $this->db->select("user_name")->get_where("qalame_user",array('user_id'=>$aa['data']->user_id))->row();
							   		$devicetoken = $this->db->select("user_device_type,user_device_token")->get_where("qalame_user",array('user_id'=>$json_array->following_id,'user_device_token !='=>''))->row();
					    			if($checkusr->account_mode==1)
					    			{
					    				$runquery = $this->common_model->common_insert("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$json_array->following_id,'create_date'=>date('Y-m-d H:i:s'),'request_status'=>2));
							   			$msg = $selectname->user_name.' sent request for follow you.';
										$requeststatus = 2;
					    				$type = 5;
					    			}else
					    			{
					    				$runquery = $this->common_model->common_insert("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$json_array->following_id,'create_date'=>date('Y-m-d H:i:s'),'request_status'=>1));
							   			$msg = $selectname->user_name.' followed you.';
										$requeststatus = 1;
					    				$type = 4;
					    			}
				    		   		$selectnoti = $this->db->select('notification_id')->get_where('qalame_notification',array('sender_id'=>$aa['data']->user_id,'receiver_id'=>$json_array->following_id,'type'=>$type))->row();
				    		   		if(empty($selectnoti))
				    		   		{
					    		   		if(!empty($devicetoken)) //notification code\\\
								   		{
			   								$massage = array('post_id'=>$aa['data']->user_id,'type'=>$type,'msg'=>$msg,'notificationTYPE'=>'SERVER');
											if($devicetoken->user_device_type=='android')
											{
												$this->common_model->sendPushNotification($devicetoken->user_device_token,$massage);
												$insernoti = $this->db->insert("qalame_notification",array('sender_id'=>$aa['data']->user_id,'receiver_id'=>$json_array->following_id,'type'=>$type,'post_id'=>0,'create_date'=>date('Y-m-d H:i:s')));
											}else
											{
												//IOS notification
											}
								   		}
				    		   		}
			   						$status = 1;
									$msg = constant("follow_".$json_array->language."_3");
					    		}else
					    		{
					    			$status = 2;
					    			//already followed
					    		}
					    	}else
					    	{
					    		if($api_type==2)
				    			{
				   					$checmode = $this->db->select('account_mode')->get_where("qalame_user",array('user_id'=>$aa['data']->user_id))->row();
				    				if($checmode->account_mode == 1)
				    				{
				    					$runquery = $this->common_model->deleteData("qalame_follow_user",array('follower_id'=>$json_array->following_id,'following_id'=>$aa['data']->user_id));
										$status = 1;
										$msg = constant("follow_".$json_array->language."_4");
										$requeststatus = 0;
				    				}else
				    				{
										$status = 1;
				    					$runquery = false;
				    				}
								}else
								{
									if(!empty($selefollow))
						    		{
						    			$runquery = $this->common_model->deleteData("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$json_array->following_id));
						    			
						    			$status = 1;
										$msg = constant("follow_".$json_array->language."_4");
										$requeststatus = 0;
						    		}else
						    		{
										$status = 2;
						    		}
								}
					    	}
					    	if($status==1)
					    	{
						    	if($runquery!=false)
				    			{
				    				$this->db->where('following_id', $aa['data']->user_id);
				    				$this->db->where('request_status', 1);
									$follower_count = $this->db->count_all_results('qalame_follow_user');	//follower count
				    				
				    				$this->db->where('follower_id', $aa['data']->user_id);
				    				$this->db->where('request_status', 1);
									$following_count = $this->db->count_all_results('qalame_follow_user');	//following count

				    				$final_output['status'] = 'success';
									$final_output['message'] = $msg;
									$final_output['data'] = array('follower_count'=>$follower_count,'following_count'=>$following_count,'request_status'=>$requeststatus);
				    			}else
				    			{
				    				$final_output['status'] = 'failed';
									$final_output['message'] = constant("some_error_".$json_array->language."");
				    			}
					    		
					    	}else
				    		{
				    			$final_output['status'] = 'failed';
								$final_output['message'] = constant("follow_".$json_array->language."_2");
				    		}
					  	}else
						{
							$final_output['status'] = 'failed';
							$final_output['message'] = constant("follow_".$json_array->language."_1");
						}
				   	}else
				   	{ 
						$final_output['status'] = 'failed';
				   		$final_output['message'] = constant("follow_".$json_array->language."_5");
				   	}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Follow and unfollow to user (Y)

	function follow_following_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); //type 1=follow, 2=following
			if(!empty($json_array->language) && !empty($json_array->type) && $json_array->type!=0) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$id = $json_array->userid;
					// if($userid!=0 && $userid!='')
					// {	
					// 	$id = $userid;
					// }else
					// {
					// 	$id = $aa['data']->user_id;
					// }
				  	if($json_array->type==1)
				  	{
				  		$sellist = $this->common_model->getDataField("user_name,user_image,user_id,account_mode,user_location,request_status","qalame_follow_user",array('following_id'=>$id,'request_status'=>1,'admin_status'=>1),'qalame_follow_user.follow_id','DESC',array('qalame_user'=>'qalame_user.user_id=qalame_follow_user.follower_id'));
				   	}else
				  	{	
				  		$sellist = $this->common_model->getDataField("user_name,user_image,user_id,user_location,account_mode,request_status","qalame_follow_user",array('follower_id'=>$id,'request_status'=>1,'admin_status'=>1),'qalame_follow_user.follow_id','DESC',array('qalame_user'=>'qalame_user.user_id=qalame_follow_user.following_id'));
				  	}
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$key->user_location))->row();
							if(!empty($locationame))
							{
								$l_name = $locationame->name;
							}else
							{
								$l_name = '';
							}
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}

							$reqstatus = $this->db->query("SELECT request_status FROM qalame_follow_user WHERE follower_id = ".$aa['data']->user_id." AND following_id = ".$key->user_id."")->row();
							if(!empty($reqstatus))
							{
								$key->following_status = $reqstatus->request_status;
							}else
							{
								$key->following_status = 0;
							}
						
							unset($key->user_location);
							//$key->following_status = $key->request_status;
							$key->user_image = $image;
							$key->location_name = $l_name;
							$arr[] = $key;
						}
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("followlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Follow and following list (Y)

	function user_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$sellist = $this->common_model->getDataField("user_email,user_name,user_id,user_image,user_location,account_mode,create_date","qalame_user",array('admin_status'=>1,'user_status'=>1),'user_name','ASC');
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$key->user_location))->row();
							if(!empty($locationame))
							{
								$l_name = $locationame->name;
							}else
							{
								$l_name = '';
							}
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}
							$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->user_id))->row();
							if(!empty($restatus))
							{
								$r_status = $restatus->request_status;
							}else
							{
								$r_status = 0;
							}

							$key->location_name = $l_name;
							$key->user_image = $image;
							$key->following_status = $r_status;
							unset($key->user_location);
							//unset($key->user_id);
							$arr[] = $key;
						} 
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("followlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end user list all data (Y)

	function user_list_loadmore()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				    $createat = $json_array->create_date;
				    $date = '';
				    if($createat != 0)
				    {
				    	$date = "AND create_date < '$createat'";
				    }
				  	//$sellist = $this->common_model->getDataField("user_name,user_id,user_image,user_location","qalame_user",array('admin_status'=>1,'user_status'=>1),'qalame_user.user_id','DESC');
				  	$sellist = $this->db->query("SELECT user_email,user_name,user_id,user_image,user_location,account_mode,create_date FROM qalame_user WHERE admin_status= 1 AND user_status=1 ".$date." ORDER BY create_date DESC LIMIT 15")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$key->user_location))->row();
							if(!empty($locationame))
							{
								$l_name = $locationame->name;
							}else
							{
								$l_name = '';
							}
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}
							$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->user_id))->row();
							if(!empty($restatus))
							{
								$r_status = $restatus->request_status;
							}else
							{
								$r_status = "0";
							}

							$key->location_name = $l_name;
							$key->user_image = $image;
							$key->following_status = $r_status;
							unset($key->user_location);
							//unset($key->user_id);
							$arr[] = $key;
						} 
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("followlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end user list load more data (Y)

	function user_list_pulltorefresh()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				    $createat = $json_array->create_date;
				    $date = '';
				    if($createat != 0)
				    {
				    	$date = "AND create_date > '$createat'";
				    }
				  	//$sellist = $this->common_model->getDataField("user_name,user_id,user_image,user_location","qalame_user",array('admin_status'=>1,'user_status'=>1),'qalame_user.user_id','DESC');
				  	$sellist = $this->db->query("SELECT user_email,user_name,user_id,user_image,user_location,account_mode,create_date FROM qalame_user WHERE admin_status= 1 AND user_status=1 ".$date." ORDER BY create_date ASC")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$key->user_location))->row();
							if(!empty($locationame))
							{
								$l_name = $locationame->name;
							}else
							{
								$l_name = '';
							}
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}
							$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->user_id))->row();
							if(!empty($restatus))
							{
								$r_status = $restatus->request_status;
							}else
							{
								$r_status = "0";
							}

							$key->location_name = $l_name;
							$key->user_image = $image;
							$key->following_status = $r_status;
							unset($key->user_location);
							//unset($key->user_id);
							$arr[] = $key;
						} 
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("followlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end user list pull to refresh data (Y)

	function user_list_pulltorefresh_date()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				    $createat = $json_array->create_date;
				    $date = '';
				    //$limit = "LIMIT 15";
				    if($createat != 0)
				    {
				    	$date = "AND create_date >= '$createat'";
				    	//$limit = '';
				    }
				  	//$sellist = $this->common_model->getDataField("user_name,user_id,user_image,user_location","qalame_user",array('admin_status'=>1,'user_status'=>1),'qalame_user.user_id','DESC');
				  	$sellist = $this->db->query("SELECT user_name,user_id,user_image,user_location,account_mode,create_date FROM qalame_user WHERE admin_status= 1 AND user_status=1 ".$date." ORDER BY create_date DESC")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$key->user_location))->row();
							if(!empty($locationame))
							{
								$l_name = $locationame->name;
							}else
							{
								$l_name = '';
							}
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}
							$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->user_id))->row();
							if(!empty($restatus))
							{
								$r_status = $restatus->request_status;
							}else
							{
								$r_status = "0";
							}

							$key->location_name = $l_name;
							$key->user_image = $image;
							$key->following_status = $r_status;
							unset($key->user_location);
							//unset($key->user_id);
							$arr[] = $key;
						} 
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("followlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end user list pull to refresh data (Y)

	function change_account_mode()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$sellist = $this->common_model->getDataField("account_mode","qalame_user",array('user_id'=>$aa['data']->user_id));
				  	if(!empty($sellist))
					{
						if($sellist[0]->account_mode == $json_array->account_mode)
						{
								$final_output['status'] = 'success';
								$final_output['message'] = "Successfully";
						}else
						{
							$update = $this->common_model->updateData('qalame_user',array('account_mode'=>$json_array->account_mode),array('user_id'=>$aa['data']->user_id));
							if($update == true)
							{
								if($json_array->account_mode == 0)
								{
									$updaterequest = $this->common_model->updateData('qalame_follow_user',array('request_status'=>1),array('following_id'=>$aa['data']->user_id,'request_status'=>2));
								}
								$final_output['status'] = 'success';
								$final_output['message'] = "Successfully";
								//$final_output['data'] = $arr;
							}else
							{
								$final_output['status'] = 'failed';
								$final_output['message'] = constant("some_error_".$json_array->language."");
							}
						}
				  	}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Follow and following list (Y)

	function accept_reject_request()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array->language))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    $userid = $json_array->userid;
				    $user_id = $aa['data']->user_id;
				    $request_key = $json_array->request_key; //1 = accept, 0=reject
				    $type = $json_array->type; //1 = follower, 2=following

				    	$selepas = $this->db->select('user_id')->get_where("qalame_user",array('user_id'=>$userid,'admin_status'=>1))->row();
				    	if(!empty($selepas))
				    	{
					    	if($type==1)
					    	{
					    		$sel_req = $this->db->query("SELECT follow_id,request_status FROM qalame_follow_user WHERE following_id ='$userid' AND follower_id ='$user_id'")->row();
					    	}else
					    	{
					    		$sel_req = $this->db->query("SELECT follow_id,request_status FROM qalame_follow_user WHERE follower_id ='$userid' AND following_id ='$user_id'")->row();
					    	}
					    	if(empty($sel_req))
					    	{	
					    		$updatepass = 'failed';
					    		$r_status = 0;
							}else
					    	{
					    		if($sel_req->request_status==2)
					    		{
						    		if($request_key == 1)
						    		{
						    			$updatepass= $this->common_model->updateData("qalame_follow_user",array('request_status'=>$request_key,'update_date'=>date('Y-m-d H:i:s')),array('follow_id'=>$sel_req->follow_id));
						    		}else
						    		{
						    			$updatepass= $this->db->delete("qalame_follow_user",array('follow_id'=>$sel_req->follow_id));
						    		}
						    		$r_status = $request_key;
					    		}else
					    		{
					    			$updatepass = 'failed';
					    			$r_status = 1;
					    		}		

					    	}
				    		$this->db->where('following_id', $user_id);
							$this->db->where('request_status', 1);
							$follower_count = $this->db->count_all_results('qalame_follow_user');	//follower count
		    				
		    				$this->db->where('follower_id', $user_id);
							$this->db->where('request_status', 1);
							$following_count = $this->db->count_all_results('qalame_follow_user');	//following count
				    		
					    	if($updatepass == 'true')
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = "";
								$final_output['data'] = array('follower_count'=>$follower_count,'following_count'=>$following_count);
		    					$final_output['request_status'] = $r_status;
					    	}elseif($updatepass=='failed')
					    	{
					    		if($r_status == 1)
					    		{
					    			$msg = 'Request already accepted';
					    		}else
					    		{
					    			$msg = 'Request already rejected';
					    		}
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = $msg;
								$final_output['data'] = array('follower_count'=>$follower_count,'following_count'=>$following_count);
		    					$final_output['request_status'] = $r_status;
					    	}
					    	else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = constant("some_error_".$json_array->language."");
					    	}
				    	}else
				    	{
				    		$final_output['status'] = 'failed';
							$final_output['message'] = constant("follow_".$json_array->language."_1");
				    	}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end accept reject request (Y)

	function user_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array->language))
			{
				if($aa['data']->admin_status ==1)
				{
					$userid = $json_array->userid;
					$user_id = $aa['data']->user_id;
					$seleuserdata = $this->common_model->common_getRow('qalame_user',array('user_id'=>$userid,'admin_status'=>1));
		    		if(!empty($seleuserdata))
		    		{
				   		$image = '';
						if(!empty($seleuserdata->user_image))
	    				{
	    					$image = base_url().'uploads/user_image/'.$seleuserdata->user_image;
	    				}
	    				$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$seleuserdata->user_location))->row();
						if(!empty($locationame))
						{
							$l_name = $locationame->name;
						}else
						{
							$l_name = '';
						}

						$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$user_id,'following_id'=>$userid))->row();
						if(!empty($restatus))
						{
							$r_status = $restatus->request_status;
						}else
						{
							$r_status = "0";
						}
						
	    				$this->db->where('user_id', $seleuserdata->user_id);
	    				$this->db->where('post_status', 1);
						$postcount = $this->db->count_all_results('qalame_user_post');	//post count
	    				
	    				$this->db->where('following_id', $seleuserdata->user_id);
						$this->db->where('request_status', 1);
						$follower_count = $this->db->count_all_results('qalame_follow_user');	//follower count
	    				
	    				$this->db->where('follower_id', $seleuserdata->user_id);
						$this->db->where('request_status', 1);
						$following_count = $this->db->count_all_results('qalame_follow_user');	//following count
					
						$object = array(
							'user_id'=>$seleuserdata->user_id,
							'user_name'=>$seleuserdata->user_name,
							'user_image'=>$image,
							'user_email'=>$seleuserdata->user_email,
							//'user_dob'=>$seleuserdata->user_dob,
							//'user_gender'=>$seleuserdata->user_gender,
							'post_count'=>$postcount,
							'follower_count'=>$follower_count,
							'following_count'=>$following_count,
							'account_mode'=>$seleuserdata->account_mode,
							'location_name'=>$l_name,
							'following_status'=>$r_status
							);
						$final_output['status'] = 'success';
						$final_output['message'] = 'Successfully';
						$final_output['data'] = $object;
	    			}else
	    			{
	    				$final_output['status'] = 'failed';
						$final_output['message'] = constant("follow_".$json_array->language."_1");
    				}
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	// User profile detail (Y)
	
	function follower_following_count()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array->language))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
					$this->db->where('following_id',$aa['data']->user_id);
					$this->db->where('request_status', 1);
					$follower_count = $this->db->count_all_results('qalame_follow_user');	//follower count
    				
    				$this->db->where('follower_id', $aa['data']->user_id);
					$this->db->where('request_status', 1);
					$following_count = $this->db->count_all_results('qalame_follow_user');	//following count

					$this->db->where('user_id', $aa['data']->user_id);
					$this->db->where('post_status', 1);
					$postcount = $this->db->count_all_results('qalame_user_post');	//post count

					$this->db->where('following_id',$aa['data']->user_id);
					$this->db->where('request_status', 2);
					$request_count = $this->db->count_all_results('qalame_follow_user');	//Request count

					$final_output['status'] = 'success';
					$final_output['message'] = "Successfully";
					$final_output['data'] = array('follower_count'=>$follower_count,'following_count'=>$following_count,'post_count'=>$postcount,"request_count"=>$request_count);

				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Follower and following count (Y)

	function get_page_content()
	{
		/*$aa = $this->check_authentication();
		if($aa['status']=='true')
		{*/
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if(!empty($json_array->language))
			{
				/*if($aa['data']->admin_status ==1){*/
				    $final_output = array();
				    $page_id = $json_array->page_id; // 1=contact us, 2 = terms, 3 = privacy
					$page_detail = $this->db->select('page_detail')->get_where('qalame_page_content',array('page_id'=>$page_id))->row();
					//$page_detail = $this->common_model->common_getRow("qalame_page_content",array('page_id'=>$page_id));
					if(!empty($page_detail))
					{
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $page_detail->page_detail;
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("some_error_".$json_array->language."");
					}
				/*}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}*/
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		/*}else
		{
			$final_output = $aa;
		}*/
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end Follower and following count (Y)

	/*function post()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$language = $this->input->post('language');
			if(!empty($language))
			{
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				   	$title = $this->input->post('title');
				   	$type = $this->input->post('type'); //1=image,2=video, 3=file
				   	$user_id = $aa['data']->user_id;
				   	$insertpost = false;
					if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name']))
					{
                   		if($type==2)
		                {	
        	            	$uploadPath = 'uploads/post/video';
		                }else
		                {
        	            	$uploadPath = 'uploads/post/image';
		                }
		                $date = date("ymdhis"); 	
                        $config['upload_path'] = $uploadPath;
                        $config['allowed_types'] = '*';
                        $config['max_size']      = ''; 
		                $config['max_width'] = '';
		                $config['max_height'] = '';

                        $subFileName = explode('.',$_FILES['file']['name']);
                        $ExtFileName = end($subFileName);

              	    	$config['file_name'] = md5($date.$_FILES['file']['name']).'.'.$ExtFileName;

                    	$fileName = $config['file_name'];
                   		$this->load->library('upload', $config);
                   		$this->upload->initialize($config);
               		  	if($this->upload->do_upload('file'))
                   		{
                    	  $fileData = $this->upload->data();
                    	  $uploadData['file_name'] = $fileData['file_name'];
                   			// shell_exec("ffmpeg -i ".base_url().'uploads/post/video'."  -y -an -sameq -f image2 -s 400x270 uploads/post/video/".$uploadData['file_name']."tumbnail.".$ExtFileName);
				       		//$aa =  exec("ffmpeg -i ".$fileData['full_path']." ".$fileData['file_path'].$fileData['raw_name'].'thumb.'.'jpeg'); 
                   		  $insertpost = $this->db->insert("qalame_user_post",array("title"=>$title,"image"=>$fileName,"file_type"=>$type,"user_id"=>$user_id,"create_date"=>date('Y-m-d h:i:s'),"update_date"=>date('Y-m-d h:i:s')));	
                   		}
                   		if($insertpost==true)
                    	{
                    		$final_output['status'] = 'success';
							$final_output['message'] = "Successfully";
							//$final_output['data'] = array("title"=>$title,"image"=>base_url().$uploadPath.'/'.$uploadData['file_name'],"file_type"=>$type,"user_id"=>$user_id);
                    	}else
                    	{
							$final_output['status'] = 'failed';
							$final_output['message'] = constant("some_error_".$language."");
                    	}
					}else
					{
						$final_output['status'] = 'failed';
	    				$final_output['message'] = "No Request Parameter Found.";
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}*/
	//end post (Y)

	function post()
    {
        $aa = $this->check_authentication();
        if($aa['status']=='true')
        {
            $language = $this->input->post('language');
            if(!empty($language))
            {
                if($aa['data']->admin_status ==1){
                    $final_output = array();
                    $title = $this->input->post('title');
                   	$user_area_interest = $this->input->post('user_area_interest');
                    $type = $this->input->post('type'); //1=image,2=video, 3=file
                    $user_id = $aa['data']->user_id;
                    $insertpost = false; $fileName = $thumb ='';
                    if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name']))
                    {
                        if($type==2)
                        {   
                            $uploadPath = 'uploads/post/video';
                        }else
                        {
                            $uploadPath = 'uploads/post/image';
                        }
                        $date = date("ymdhis");     
                        $config['upload_path'] = $uploadPath;
                        $config['allowed_types'] = '*';
                        $config['max_size']  = ''; 
                        $config['max_width'] = '';
                        $config['max_height'] = '';

                        $subFileName = explode('.',$_FILES['file']['name']);
                        $ExtFileName = end($subFileName);

                        $config['file_name'] = md5($date.$_FILES['file']['name']).'.'.$ExtFileName;

                        $fileName = $config['file_name'];
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('file'))
                        {
                        	
	                        $fileData = $this->upload->data();
	                        $uploadData['file_name'] = $fileData['file_name'];
                            // shell_exec("ffmpeg -i ".base_url().'uploads/post/video'."  -y -an -sameq -f image2 -s 400x270 uploads/post/video/".$uploadData['file_name']."tumbnail.".$ExtFileName);
                            //$aa =  exec("ffmpeg -i ".$fileData['full_path']." ".$fileData['file_path'].$fileData['raw_name'].'thumb.'.'jpeg'); 
	                        if($type != 1 && isset($_FILES["preview"]["name"]))
	                    	{
	                        	$uploadPath1 = './uploads/post/preview';
	                    		$date = date("ymdhis");     
		                        $config['upload_path'] = $uploadPath1;
		                        $config['allowed_types'] = '*';
		                        $config['max_size']  = ''; 
		                        $config['max_width'] = '';
		                        $config['max_height'] = '';
	                    		
	                    		$thumurl=$_FILES["preview"]["name"];
				    		    $config['file_name'] = militime.'_'.$thumurl;
				    		    $thumb = $config['file_name'];
		                        $this->load->library('upload', $config);
		                        $this->upload->initialize($config);
		                        if($this->upload->do_upload('preview'))
		                        {
		                            $fileData1 = $this->upload->data();
			                        $uploadData1['file_name'] = $fileData1['file_name'];
		                        }
				                //move_uploaded_file($_FILES["preview"]["tmp_name"],$uploadPath1.$thumb);
	                    	}	
                        }
                    }
                    if($title =='' && $fileName == '')
                    {
                    	$final_output['status'] = 'failed';
                		$final_output['message'] = "No Request Parameter Found.";
                    }else
                    {
	                    $insertpost = $this->db->insert("qalame_user_post",array("title"=>$title,"image"=>$fileName,'preview'=>$thumb,"file_type"=>$type,'user_interest'=>$user_area_interest,"user_id"=>$user_id,"create_date"=>date('Y-m-d h:i:s'),"update_date"=>date('Y-m-d h:i:s'))); 
	                    if($insertpost==true)
	                    {
	                    	$myname = $this->db->select('user_name')->get_where('qalame_user',array('user_id'=>$user_id))->row();
	                    	$msg = $myname->user_name.' added new post.';
	                       	$pushMessage = array('post_id'=>$this->db->insert_id(),'msg'=>$msg,'type'=>1,'notificationTYPE'=>'SERVER');
	                       	$sellist = $this->db->query("SELECT qalame_user.user_device_token,qalame_user.user_device_type,qalame_user.user_id FROM qalame_user where qalame_user.user_id IN (select follower_id from qalame_follow_user where following_id = '$user_id' AND request_status = 1) AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 AND qalame_user.user_id != '$user_id' AND qalame_user.user_device_token != ''")->result_array();
	                       	if(!empty($sellist))
	                       	{
	                       			$gcmRegIds = array();
						          	$i = 0;
							        foreach ($sellist as $query_row) 
							        {
							            if($query_row['user_device_type']=='android')
							            {
							              $i++;
							              $gcmRegIds[floor($i/1000)][] = $query_row['user_device_token'];
							              $insert_data[] = '("","'.$user_id.'","'.$query_row['user_id'].'","1","'.$this->db->insert_id().'","0","'.datetime.'")';
							            }else
							            {
							            	//IOS notification
							            }
							        }
							        if(isset($gcmRegIds)) 
							        {
							          $message = $pushMessage;
							          $pushStatus = array();
							          foreach($gcmRegIds as $val) $pushStatus[] = $this->common_model->sendNotification($val, $message);
							        }

							        $notification_data = implode(',',$insert_data);

							        $insertt = $this->db->query("INSERT INTO qalame_notification VALUES $notification_data");
							        //$insernoti = $this->db->insert("qalame_notification",array('sender_id'=>$user_id,'receiver_id'=>$select[0]->user_id,'type'=>2,'post_id'=>$post_id,'create_date'=>date('Y-m-d H:i:s')));

	                       	}
	                        $final_output['status'] = 'success';
	                        $final_output['message'] = "Successfully";
	                        //$final_output['data'] = array("title"=>$title,"image"=>base_url().$uploadPath.'/'.$uploadData['file_name'],"file_type"=>$type,"user_id"=>$user_id);
	                    }else
	                    {
	                        $final_output['status'] = 'failed';
	                        $final_output['message'] = constant("some_error_".$language."");
	                    }
                    }
                }else{
                    $final_output['status'] = 'failed';
                    $final_output['message'] = constant("admin_status_".$language."");
                }
            }else
            {
                $final_output['status'] = 'failed';
                $final_output['message'] = "No Request Parameter Found.";
            }
        }else
        {
            $final_output = $aa;
        }
        header("content-type: application/json");
        echo json_encode($final_output);
    }
    
	function post_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); //type 1=follow, 2=following
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$user_id = $aa['data']->user_id;
				 	$create_date = $json_array->create_date;
				 	$load_more = '';
				 	if($create_date!=0)
				 	{
				 		$load_more = "AND qalame_user_post.create_date < '$create_date'";
				 	}
				 	/*$intt = 1;
				 	$interest = $this->db->select('user_area_interest')->get_where("qalame_user",array('user_id'=>$user_id))->row();
				 	if(!empty($interest))
				 	{
				 		$intt = $interest->user_area_interest;
				 	}*/
				  	//OLD API $sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.update_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where (qalame_user_post.user_id = '$user_id' OR qalame_user_post.user_id IN (select following_id from qalame_follow_user where follower_id = '6' AND request_status = 1) OR qalame_user_post.user_interest IN ($intt)) AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.update_date DESC LIMIT 15")->result();
				  	$sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_id = qalame_user.user_id LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where (qalame_user_post.user_id = '$user_id' OR qalame_user_post.user_id IN (select following_id from qalame_follow_user where follower_id = '$user_id' AND request_status = 1) ) AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC LIMIT 15")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}

							if($key->file_type == 2)
							{
								$url = base_url().'uploads/post/video/'.$key->image;
							}else
							{
								$url = base_url().'uploads/post/image/'.$key->image;
							}
							if($key->file_type != 1)
							{
								$previewurl = base_url().'uploads/post/preview/'.$key->preview;
							}else
							{
								$previewurl = '';
							}

							if($user_id != $key->user_id)
							{
								$key->following_status = 1;
							}else
							{
								$key->following_status = 0;
							}
							if($key->location_name==null)
							{
								$key->location_name = '';
							}
							$this->db->where('post_id', $key->post_id);
							$this->db->where('status', 1);
							$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//following count

							$this->db->where('post_id', $key->post_id);
							$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//following count

							$like_status = $this->db->select('like_id')->get_where('qalame_like_unlike_post',array('user_id'=>$user_id,'post_id'=>$key->post_id,'status'=>1))->row();
							if(!empty($like_status))
							{
								$likestatus = 1;
							}else
							{
								$likestatus = 0;							
							}
							unset($key->user_location);
							unset($key->image);
							$key->show_date = strtoupper(date("Y M d",strtotime($key->create_date)));
							$key->user_image = $image;
							$key->post_image = $url;
							$key->preview = $previewurl;
							$key->like_count = $like_count;
							$key->comment_count = $comment_count;
							$key->user_like_status = $likestatus;
							$arr[] = $key;
						}
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("postlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end post list load more(Y)

	function post_list_pulltorefresh()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); //type 1=follow, 2=following
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$user_id = $aa['data']->user_id;
				 	$create_date = $json_array->create_date;
				 	$load_more = '';
				 	if($create_date!=0)
				 	{
				 		$load_more = "AND qalame_user_post.create_date >= '$create_date'";
				 	}
				 	/*$intt = 1;
				 	$interest = $this->db->select('user_area_interest')->get_where("qalame_user",array('user_id'=>$user_id))->row();
				 	if(!empty($interest))
				 	{
				 		$intt = $interest->user_area_interest;
				 	}*/
				  	//OLD API $sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.update_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where (qalame_user_post.user_id = '$user_id' OR qalame_user_post.user_id IN (select following_id from qalame_follow_user where follower_id = '6' AND request_status = 1) OR qalame_user_post.user_interest IN ($intt)) AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.update_date DESC LIMIT 15")->result();
				  	$sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_id = qalame_user.user_id LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where (qalame_user_post.user_id = '$user_id' OR qalame_user_post.user_id IN (select following_id from qalame_follow_user where follower_id = '$user_id' AND request_status = 1) ) AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}

							if($key->file_type == 2)
							{
								$url = base_url().'uploads/post/video/'.$key->image;
							}else
							{
								$url = base_url().'uploads/post/image/'.$key->image;
							}
							if($key->file_type != 1)
							{
								$previewurl = base_url().'uploads/post/preview/'.$key->preview;
							}else
							{
								$previewurl = '';
							}

							if($user_id != $key->user_id)
							{
								$key->following_status = 1;
							}else
							{
								$key->following_status = 0;
							}
							if($key->location_name==null)
							{
								$key->location_name = '';
							}
							$this->db->where('post_id', $key->post_id);
							$this->db->where('status', 1);
							$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//following count

							$this->db->where('post_id', $key->post_id);
							$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//following count

							$like_status = $this->db->select('like_id')->get_where('qalame_like_unlike_post',array('user_id'=>$user_id,'post_id'=>$key->post_id,'status'=>1))->row();
							if(!empty($like_status))
							{
								$likestatus = 1;
							}else
							{
								$likestatus = 0;							
							}
							unset($key->user_location);
							unset($key->image);
							$key->show_date = strtoupper(date("Y M d",strtotime($key->create_date)));
							$key->user_image = $image;
							$key->post_image = $url;
							$key->preview = $previewurl;
							$key->like_count = $like_count;
							$key->comment_count = $comment_count;
							$key->user_like_status = $likestatus;
							$arr[] = $key;
						}
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("postlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end post list pull to refresh(Y)
	function Like_unlike_post()
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				  	$post_id = $json_array->post_id;
				  	$like_status = $json_array->status; //1=like, 0=unlike
				  	$user_id = $aa['data']->user_id;
					$final_output = array();
					if(!empty($post_id) && $post_id != 0)
				   	{
				   		$select = $this->common_model->getDataField("post_id,user_id","qalame_user_post",array('post_id'=>$post_id,'post_status'=>1));
				   		if($select)
				   		{
		   					$seluservote = $this->db->query("SELECT like_id,status FROM qalame_like_unlike_post WHERE post_id ='$post_id' AND user_id='$user_id'")->row();
		   					if(!empty($seluservote))
				   			{
			   					if($like_status != $seluservote->status)
			   					{
		   							$unlike = $this->common_model->updateData("qalame_like_unlike_post",array('status'=>$like_status,'create_date'=>date('Y-m-d H:i:s')),array('like_id'=>$seluservote->like_id));
			   					
			   						if($unlike==TRUE)
			   						{
				   						$response = 'true';
							    	}else
						   	 		{
				   						$response = 'failed';
							  		}
				   				}else{
				   					$response = 'false';
						        }	
		   					}else
		   					{
		   						if($like_status== 1)
			   					{
		   							$likes = $this->common_model->common_insert("qalame_like_unlike_post",array('post_id'=>$post_id,'user_id'=>$user_id,'status'=>$like_status,'create_date'=>date('Y-m-d H:i:s')));
			   						if($likes==TRUE)
			   						{
			   							//send notification
			   							$response = 'true';
								   		if($user_id != $select[0]->user_id)
								   		{
									   		$selectname = $this->db->select("user_name")->get_where("qalame_user",array('user_id'=>$user_id))->row();
									   		$devicetoken = $this->db->select("user_device_type,user_device_token")->get_where("qalame_user",array('user_id'=>$select[0]->user_id,'user_device_token !='=>''))->row();
									   		if(!empty($devicetoken))
									   		{
									   			$msg = $selectname->user_name.' liked your post.';
				   								$massage = array('post_id'=>$post_id,'type'=>2,'msg'=>$msg,'notificationTYPE'=>'SERVER');
												if($devicetoken->user_device_type=='android')
												{
													$this->common_model->sendPushNotification($devicetoken->user_device_token,$massage);
													$insernoti = $this->db->insert("qalame_notification",array('sender_id'=>$user_id,'receiver_id'=>$select[0]->user_id,'type'=>2,'post_id'=>$post_id,'create_date'=>date('Y-m-d H:i:s')));
												}else
												{
													//IOS notification
												}
									   		}
				   						}
				   					}else
						   	 		{
				   						$response = 'failed';
							  		}
				   				}else{
				   					$response = 'false';
				   		        }	
		   					}
		   					if($response != 'failed')
		   					{
		   						$this->db->where('post_id',$post_id);
		   						$this->db->where('status',1);
								$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//post like count


								$this->db->where('post_id',$post_id);
								$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//post like count
		   				
		   						if($response=='true')
		   						{
		   							if($like_status == 1){ $msg = 'Successfully Liked'; }else{ $msg = 'Successfully Unliked'; }
		   							$final_output['status'] = 'success';
						   	  		$final_output['message'] = $msg;
						   	  		$final_output['data'] = array('like_count'=>$like_count,'comment_count'=>$comment_count);
		   						}else
		   						{
		   							if($like_status == 1){ $msg = 'Already Liked'; }else{ $msg = 'Already Unliked'; }
		   							$final_output['status'] = 'success';
						   	  		$final_output['message'] = $msg;
						   	  		$final_output['data'] = array('like_count'=>$like_count,'comment_count'=>$comment_count);
		   						}
		   					}else
		   					{
				   	 			$final_output['status'] = 'failed';
				   	  			$final_output['message'] = constant("some_error_".$json_array->language."");
		   					}
			   	   		}else
				   		{
				   			$final_output['status'] = 'delete_post';
			   	  			$final_output['message'] = 'Post not available';
				   		}
				   	}else
				   	{
						$final_output['status'] = 'failed';
		   	  			$final_output['message'] = 'Invalid Request parameter';
			       	}
	   			}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end like unlike post

	function comment_on_post()
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				  	$post_id = $json_array->post_id;
				  	$comment = $json_array->comment; //1=like, 0=unlike
				  	$user_id = $aa['data']->user_id;
					$final_output = array();
					if(!empty($post_id) && $post_id != 0)
				   	{
					   	$select = $this->common_model->getDataField("post_id,user_id","qalame_user_post",array('post_id'=>$post_id,'post_status'=>1));
				   		if($select)
				   		{	
					   		$likes = $this->common_model->common_insert("qalame_user_post_comment",array('post_id'=>$post_id,'user_id'=>$user_id,'comment'=>$comment,'create_date'=>date('Y-m-d H:i:s')));
	   						if($likes==TRUE)
	   						{
	   							$commentid = $this->db->insert_id();
	   							//send notification
	   							if($user_id != $select[0]->user_id)
	   							{
		   							$selectname = $this->db->select("user_name")->get_where("qalame_user",array('user_id'=>$user_id))->row();
							   		$devicetoken = $this->db->select("user_device_type,user_device_token")->get_where("qalame_user",array('user_id'=>$select[0]->user_id,'user_device_token !='=>''))->row();
							   		if(!empty($devicetoken))
							   		{
							   			$msg = $selectname->user_name.' commented on your post.';
		   								$massage = array('post_id'=>$post_id,'type'=>3,'msg'=>$msg,'notificationTYPE'=>'SERVER');
										if($devicetoken->user_device_type=='android')
										{
											$aa = $this->common_model->sendPushNotification($devicetoken->user_device_token,$massage);
											$insernoti = $this->db->insert("qalame_notification",array('sender_id'=>$user_id,'receiver_id'=>$select[0]->user_id,'type'=>3,'comment_id'=>$commentid,'post_id'=>$post_id,'create_date'=>date('Y-m-d H:i:s')));
										}else
										{
											//IOS notification
										}
							   		}
	   							}
	   							/*$this->db->where('post_id',$post_id);
		   						$this->db->where('status',1);
								$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//post like count

								$this->db->where('post_id',$post_id);
								$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//post like count*/

		   						$final_output['status'] = 'success';
					   	  		$final_output['message'] = 'Successfully commented';
					   	  		//$final_output['data'] = array('like_count'=>$like_count,'comment_count'=>$comment_count);
		   					}else
				   	 		{
				   	 			$final_output['status'] = 'failed';
				   	  			$final_output['message'] = constant("some_error_".$json_array->language."");
		   					}
				   		}else
				   		{
				   			$final_output['status'] = 'delete_post';
			   	  			$final_output['message'] = 'Post not available';
				   		}
				   	}else
				   	{
						$final_output['status'] = 'failed';
		   	  			$final_output['message'] = 'Invalid Request parameter';
			       	}
	   			}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end like unlike post

	public function delete_post()
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				  	$post_id = $json_array->post_id;
				  	$user_id = $aa['data']->user_id;
					$final_output = array();
					if(!empty($post_id) && $post_id != 0)
				   	{
					   	$select = $this->common_model->getDataField("post_id","qalame_user_post",array('post_id'=>$post_id,'user_id'=>$user_id));
				   		if($select)
				   		{	
					   		$likes = $this->common_model->deleteData("qalame_user_post",array('post_id'=>$post_id,'user_id'=>$user_id));

					   		$likes = $this->common_model->deleteData("qalame_user_post_comment",array('post_id'=>$post_id));

					   		$likes = $this->common_model->deleteData("qalame_like_unlike_post",array('post_id'=>$post_id));

					   		$likes = $this->common_model->deleteData("qalame_notification",array('post_id'=>$post_id));

	   						if($likes==TRUE)
	   						{
	   							$final_output['status'] = 'success';
					   	  		$final_output['message'] = constant("post_del_".$json_array->language."_1");
					 		}else
				   	 		{
				   	 			$final_output['status'] = 'failed';
				   	  			$final_output['message'] = constant("some_error_".$json_array->language."");
		   					}
				   		}else
				   		{
				   			$final_output['status'] = 'delete_post';
			   	  			$final_output['message'] = 'Post not available';
				   		}
				   	}else
				   	{
						$final_output['status'] = 'failed';
		   	  			$final_output['message'] = 'Invalid Request parameter';
			       	}
	   			}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end delete post

	function send_otp_updateprofile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status ==1)
			{
				$json = file_get_contents('php://input');
			    $json_array = json_decode($json);
			    $final_output = array();
			    if(!empty($json_array->mobile) && !empty($json_array->code) && !empty($json_array->language))
			    {
			    	$checkotp = $this->common_model->common_getRow("qalame_user",array('user_email'=>$json_array->mobile,'user_status'=>1));
			    	if(!empty($checkotp))
			    	{	
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("update_profile_".$json_array->language."_3");
				    }else
				    {
							//$otp = $this->common_model->randomuniqueCode();
			            $otp = '1234';
			           	$msg = "Your OTP for update profile is: ".$otp;
						$updateotp = $this->common_model->updateData("qalame_user",array('v_code'=>$otp,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$aa['data']->user_id));
						if($updateotp!=false)
						{
							//Send OTP function
							$final_output['status'] = 'success';
							$final_output['message'] = constant("mobile_otp_".$json_array->language."_1");
						}else
						{
							$final_output['status'] = 'failed';
							$final_output['message'] = constant("some_error_".$json_array->language."");
						}
			    	}
			    }else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = "No Request Parameter Found.";
			    }
		    }else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$json_array->language."");	
	    	}
		}else
		{
			$final_output = $aa;
		}
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//send otp for update profile mobile

	function update_profile_usingotp()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$language = $this->input->post('language');
			if($aa['data']->admin_status==1)
			{
				$otp = $this->input->post('otp');
				$data['mobile_code'] = $this->input->post('code');
				$data['user_email'] = $this->input->post('mobile');
				$data['user_name'] = $this->input->post('user_name');
				$data['user_location'] = $this->input->post('user_location');
				$data['user_area_interest'] = $this->input->post('user_area_interest');
				$data['user_dob'] = $this->input->post('user_dob');
				$data['user_gender'] = $this->input->post('user_gender');
				$checkotp = $this->common_model->common_getRow("qalame_user",array('user_email'=>$this->input->post('mobile'),'user_status'=>1));
				if(!empty($checkotp))
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("update_profile_".$language."_3");
				}else
				{
					$checkotpnew = $this->common_model->common_getRow("qalame_user",array('user_id'=>$aa['data']->user_id));
					if($checkotpnew->v_code == $otp)
					{
						if(!empty($_FILES['user_image']['name']) && isset($_FILES['user_image']))
						{
							$config = array();
							$config['upload_path']   = './uploads/user_image/';
							$config['allowed_types'] = 'jpg|jpeg|png';

							$subFileName = explode('.',$_FILES['user_image']['name']);
							$ExtFileName = end($subFileName);
						    $config['file_name'] = md5(militime.$_FILES['user_image']['name']).'.'.$ExtFileName;

							$this->load->library('upload',$config);
					        $this->upload->initialize($config);
							if($this->upload->do_upload('user_image'))
							{ 
								$image_data = $this->upload->data();
								$data['user_image'] = $image_data['file_name'];
								$image = base_url().'uploads/user_image/'.$image_data['file_name'];
							}
						}else
						{
							$getdata = $this->common_model->common_getRow("qalame_user",array('user_id'=>$aa['data']->user_id));	
							if($getdata->user_image!=''){
							$image = base_url().'uploads/user_image/'.$getdata->user_image;
							}else{
							 $image = '';
							}
						}
						$data['v_code'] = '';
						$data['update_date'] = date('Y-m-d H:i:s');
						$update_data = $this->common_model->updateData("qalame_user",$data,array('user_id'=>$aa['data']->user_id));
						if($this->db->affected_rows())
						{
							$final_output['status'] = 'success'; 
						 	$final_output['message'] = constant("update_profile_".$language."_2");
							$final_output['data'] = array('user_image'=>$image); 
						}else
						{
							$final_output['status'] = 'failed'; 
						 	$final_output['message'] = constant("some_error_".$language."");
						}
					}else
					{
						$final_output['status'] = 'failed'; 
						$final_output['message'] = constant("mobile_verifiy_".$language."_2");
					}
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$language."");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end update profile mobile number (Y)

	function edit_post()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			$language = $json_array->language;
			if($aa['data']->admin_status==1)
			{	
				$data['title'] = $json_array->title;
				$data['user_interest'] = $json_array->user_area_interest;
				$post_id = $json_array->post_id;
				if(!empty($data['title']) && !empty($data['user_interest']))
				{
					$select = $this->common_model->getDataField("post_id","qalame_user_post",array('post_id'=>$post_id,'user_id'=>$aa['data']->user_id));
					if(!empty($select))
					{
						$data['update_date'] = date('Y-m-d H:i:s');
						$update_data = $this->common_model->updateData("qalame_user_post",$data,array('post_id'=>$post_id));
						if($this->db->affected_rows())
						{
							$final_output['status'] = 'success'; 
						 	$final_output['message'] = constant("post_edit_".$language."_1");
						}else
						{
							$final_output['status'] = 'failed'; 
						 	$final_output['message'] = constant("some_error_".$language."");
						}
					}else
					{
						$final_output['status'] = 'delete_post';
			   	  		$final_output['message'] = 'Post not available';
					}
				}else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = "No Request Parameter Found.";
			    }
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$language."");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end edit post (Y)

	function Get_post_like_comment_detail()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = $sellist = array();
				    $type = $json_array->type; //1=like, 2=comment
				    $post_id = $json_array->post_id;
				    //$sellist = $this->common_model->getDataField("user_name,user_id,user_image,user_location","qalame_user",array('admin_status'=>1,'user_status'=>1),'qalame_user.user_id','DESC');
			  		$select = $this->common_model->getDataField("post_id","qalame_user_post",array('post_id'=>$post_id,'post_status'=>1));
			   		if($select)
			   		{
					  	if($type==1)
					  	{
					  		$sellist = $this->db->query("SELECT likepost.like_id,likepost.user_id,likepost.create_date,qalame_user.user_email,qalame_user.user_name,qalame_user.user_id,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM qalame_like_unlike_post as likepost INNER JOIN qalame_user ON likepost.user_id = qalame_user.user_id LEFT JOIN location_list ON qalame_user.user_location = location_list.location_id WHERE qalame_user.admin_status= 1 AND likepost.post_id = '$post_id' ORDER BY likepost.like_id DESC")->result();
					  	}elseif($type==2)
					  	{
					  		$sellist = $this->db->query("SELECT comntpost.comment_id,comntpost.user_id,comntpost.create_date,comntpost.comment,qalame_user.user_email,qalame_user.user_name,qalame_user.user_id,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM qalame_user_post_comment as comntpost INNER JOIN qalame_user ON comntpost.user_id = qalame_user.user_id LEFT JOIN location_list ON qalame_user.user_location = location_list.location_id WHERE qalame_user.admin_status= 1 AND comntpost.post_id = '$post_id' ORDER BY comntpost.comment_id DESC")->result();
					  	}
					  	if(!empty($sellist))
						{
							foreach ($sellist as $key) {
								if($key->location_name==NULL)
								{
									$key->location_name = '';
								}
								if(!empty($key->user_image))
								{
									$image = base_url().'uploads/user_image/'.$key->user_image;
								}else
								{
									$image = '';
								}
								$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->user_id))->row();
								if(!empty($restatus))
								{
									$r_status = $restatus->request_status;
								}else
								{
									$r_status = "0";
								}
								$key->show_date = strtoupper(date("Y M d",strtotime($key->create_date)));
								$key->user_image = $image;
								$key->following_status = $r_status;
								unset($key->user_location);
								//unset($key->user_id);
								$arr[] = $key;
							} 
							$final_output['status'] = 'success';
							$final_output['message'] = "Successfully";
							$final_output['data'] = $arr;
					  	}else
						{
							$final_output['status'] = 'failed';
							$final_output['message'] = constant("post_record_".$json_array->language."_".$type);
						}
					}else
					{
						$final_output['status'] = 'delete_post';
			   	  		$final_output['message'] = 'Post not available';
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end user list load more data (Y)

	function delete_comment()
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				  	$comment_id = $json_array->comment_id;
				  	$user_id = $aa['data']->user_id;
					$final_output = array(); $response='failed';
					if(!empty($comment_id) && $comment_id != 0)
				   	{
					   	$select = $this->common_model->getDataField("comment_id,user_id,post_id","qalame_user_post_comment",array('comment_id'=>$comment_id));
				   		if(!empty($select))
				   		{	
				   			if($select[0]->user_id != $user_id)
				   			{
					   			$selpost = $this->db->select('post_id')->get_where("qalame_user_post",array('post_id'=>$select[0]->post_id,'user_id'=>$user_id))->row();
					   			if(!empty($selpost))
					   			{
				   			   		$response = $this->common_model->deleteData("qalame_user_post_comment",array('comment_id'=>$comment_id));

				   			   		$response = $this->common_model->deleteData("qalame_notification",array('comment_id'=>$comment_id));

			   					}else
					   			{
					   				$response = 'false';	
					   			}
				   			}else
				   			{
				   			   	$response = $this->common_model->deleteData("qalame_user_post_comment",array('comment_id'=>$comment_id));

				   			   	$response = $this->common_model->deleteData("qalame_notification",array('comment_id'=>$comment_id));
				   			}
				   		}else
				   		{
				   			$response = 'TRUE';
				   		}
				   		if($response=='TRUE')
				   		{ 
				   			$final_output['status'] = 'success';
					   	  	$final_output['message'] = constant("post_coment_".$json_array->language."_1");
				   	  	}elseif($response=='false')
				   	  	{
				   	  		$final_output['status'] = 'failed';
					   	  	$final_output['message'] = constant("post_coment_".$json_array->language."_2");
				   	  	}else
				   	  	{
							$final_output['status'] = 'failed';
						   	$final_output['message'] = constant("some_error_".$json_array->language."");
				   	  	}
				   	}else
				   	{
						$final_output['status'] = 'failed';
		   	  			$final_output['message'] = 'Invalid Request parameter';
			       	}
	   			}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end delete comment
	function post_list_by_interest()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$userid = $aa['data']->user_id;
				 	$create_date = $json_array->create_date;
				 	$user_id = $json_array->user_id; //blank = interest , user_id = user post
				 	$load_more = '';
				 	if($create_date!=0)
				 	{
				 		$load_more = "AND qalame_user_post.create_date < '$create_date'";
				 	}
				 	$intt = 1;
				 	$interest = $this->db->select('user_area_interest')->get_where("qalame_user",array('user_id'=>$userid))->row();
				 	if(!empty($interest))
				 	{
				 		$intt = $interest->user_area_interest;
				 	}
				  	//OLD API $sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.update_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where (qalame_user_post.user_id = '$user_id' OR qalame_user_post.user_id IN (select following_id from qalame_follow_user where follower_id = '6' AND request_status = 1) OR qalame_user_post.user_interest IN ($intt)) AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.update_date DESC LIMIT 15")->result();
				  	if(empty($user_id))
				  	{
				  		// $sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_interest IN (qalame_user.user_area_interest) LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where qalame_user_post.user_interest IN ($intt) AND  qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC LIMIT 30")->result();

				  		$sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_interest IN ($intt) LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where qalame_user_post.user_interest IN ($intt) AND  qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC LIMIT 30")->result();
				  	}else
				  	{
				  		$sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_id = qalame_user.user_id LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where qalame_user_post.user_id = '$user_id' AND  qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC LIMIT 30")->result();
				  	}
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}

							if($key->file_type == 2)
							{
								$url = base_url().'uploads/post/video/'.$key->image;
							}else
							{
								$url = base_url().'uploads/post/image/'.$key->image;
							}
							if($key->file_type != 1)
							{
								$previewurl = base_url().'uploads/post/preview/'.$key->preview;
							}else
							{
								$previewurl = '';
							}
							$followsta = $this->db->select('request_status')->get_where("qalame_follow_user",array('follower_id'=>$user_id,'following_id'=>$key->user_id,'request_status'=>1))->row();
							if(!empty($followsta))
							{
								$key->following_status = 1;
							}else
							{
								$key->following_status = 0;
							}
							if($key->location_name==null)
							{
								$key->location_name = '';
							}
							$this->db->where('post_id', $key->post_id);
							$this->db->where('status', 1);
							$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//following count

							$this->db->where('post_id', $key->post_id);
							$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//following count

							$like_status = $this->db->select('like_id')->get_where('qalame_like_unlike_post',array('user_id'=>$user_id,'post_id'=>$key->post_id,'status'=>1))->row();
							if(!empty($like_status))
							{
								$likestatus = 1;
							}else
							{
								$likestatus = 0;							
							}
							unset($key->user_location);
							unset($key->image);
							$key->show_date = strtoupper(date("Y M d",strtotime($key->create_date)));
							$key->user_image = $image;
							$key->post_image = $url;
							$key->preview = $previewurl;
							$key->like_count = $like_count;
							$key->comment_count = $comment_count;
							$key->user_like_status = $likestatus;
							$arr[] = $key;
						}
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("postlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("some_error_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end post list by interest(Y)

	function post_list_by_interest_pulltorefresh()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$userid = $aa['data']->user_id;
				 	$create_date = $json_array->create_date;
				 	$user_id = $json_array->user_id; //blank = interest , user_id = user post
				 	$load_more = '';
				 	if($create_date!=0)
				 	{
				 		$load_more = "AND qalame_user_post.create_date >= '$create_date'";
				 	}
				 	$intt = 1;
				 	$interest = $this->db->select('user_area_interest')->get_where("qalame_user",array('user_id'=>$userid))->row();
				 	if(!empty($interest))
				 	{
				 		$intt = $interest->user_area_interest;
				 	}
				  	//OLD API $sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.update_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where (qalame_user_post.user_id = '$user_id' OR qalame_user_post.user_id IN (select following_id from qalame_follow_user where follower_id = '6' AND request_status = 1) OR qalame_user_post.user_interest IN ($intt)) AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.update_date DESC LIMIT 15")->result();
				  	if(empty($user_id))
				  	{
				  		$sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_interest IN ($intt) LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where qalame_user_post.user_interest IN ($intt) AND  qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC")->result();
				  	}else
				  	{
				  		$sellist = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_id = qalame_user.user_id LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where qalame_user_post.user_id = '$user_id' AND  qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ".$load_more." GROUP BY qalame_user_post.post_id ORDER BY qalame_user_post.create_date DESC")->result();
				  	}
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}

							if($key->file_type == 2)
							{
								$url = base_url().'uploads/post/video/'.$key->image;
							}else
							{
								$url = base_url().'uploads/post/image/'.$key->image;
							}
							if($key->file_type != 1)
							{
								$previewurl = base_url().'uploads/post/preview/'.$key->preview;
							}else
							{
								$previewurl = '';
							}
							$followsta = $this->db->select('request_status')->get_where("qalame_follow_user",array('follower_id'=>$user_id,'following_id'=>$key->user_id,'request_status'=>1))->row();
							if(!empty($followsta))
							{
								$key->following_status = 1;
							}else
							{
								$key->following_status = 0;
							}
							if($key->location_name==null)
							{
								$key->location_name = '';
							}
							$this->db->where('post_id', $key->post_id);
							$this->db->where('status', 1);
							$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//following count

							$this->db->where('post_id', $key->post_id);
							$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//following count

							$like_status = $this->db->select('like_id')->get_where('qalame_like_unlike_post',array('user_id'=>$user_id,'post_id'=>$key->post_id,'status'=>1))->row();
							if(!empty($like_status))
							{
								$likestatus = 1;
							}else
							{
								$likestatus = 0;							
							}
							unset($key->user_location);
							unset($key->image);
							$key->show_date = strtoupper(date("Y M d",strtotime($key->create_date)));
							$key->user_image = $image;
							$key->post_image = $url;
							$key->preview = $previewurl;
							$key->like_count = $like_count;
							$key->comment_count = $comment_count;
							$key->user_like_status = $likestatus;
							$arr[] = $key;
						}
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("postlist_".$json_array->language."_1");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("some_error_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end post list by interest pull to refresh(Y)

	function download_file()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			$language = $json_array->language;
			if($aa['data']->admin_status==1)
			{	
				$user_id = $aa['data']->user_id;
				$post_id = $json_array->post_id;
				if(!empty($post_id))
				{
					$select = $this->common_model->getDataField("post_id,file_type,image,preview","qalame_user_post",array('post_id'=>$post_id));
					if(!empty($select))
					{
						if($select[0]->file_type==3)
						{
							$selimg = $this->db->select('download_id')->get_where('qalame_download_file',array("user_id"=>$user_id,'post_id'=>$post_id))->row();
							if(!empty($selimg))
							{
								$update_data = $this->common_model->updateData("qalame_download_file",array('download_file'=>$select[0]->image,'preview_file'=>$select[0]->preview,'create_date'=>date('Y-m-d H:i:s')),array("user_id"=>$user_id,'post_id'=>$post_id));
								
								$final_output['status'] = 'success'; 
							 	$final_output['message'] = "Successfully downloaded";	
							}else
							{
								$update_data = $this->common_model->common_insert("qalame_download_file",array('download_file'=>$select[0]->image,'preview_file'=>$select[0]->preview,'user_id'=>$user_id,'post_id'=>$post_id,'create_date'=>date('Y-m-d H:i:s')));
								if($this->db->affected_rows())
								{
									$final_output['status'] = 'success'; 
								 	$final_output['message'] = "Successfully downloaded";
								}else
								{
									$final_output['status'] = 'failed'; 
								 	$final_output['message'] = constant("some_error_".$language."");
								}
							}
						}else
						{
							$final_output['status'] = 'failed'; 
							$final_output['message'] = constant("download_".$language."_1");
						}
					}else
					{
						$final_output['status'] = 'delete_post';
			   	  		$final_output['message'] = 'Post not available';
					}
				}else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = "No Request Parameter Found.";
			    }
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$language."");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end edit post (Y)

	function download_file_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			$language = $json_array->language;
			if($aa['data']->admin_status==1)
			{	
				$user_id = $aa['data']->user_id;
				$selimg = $this->common_model->getData('qalame_download_file',array("user_id"=>$user_id),'create_date','DESC');
				if(!empty($selimg))
				{
					foreach ($selimg as $key) {
						$arr[] = array(
								'download_id'=>$key->download_id,
								'post_id'=>$key->post_id,
								'post_image'=>base_url().'uploads/post/image/'.$key->download_file,
								'preview'=>base_url().'uploads/post/preview/'.$key->preview_file,
								'create_date'=>$key->create_date
						  		);
					}
					$final_output['status'] = 'success'; 
				 	$final_output['message'] = "Successfully get files";	
				 	$final_output['data'] = $arr;	
				}else
				{
					$final_output['status'] = 'failed'; 
				 	$final_output['message'] = constant("download_".$language."_2");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = constant("admin_status_".$language."");
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end edit post (Y)

	function delete_file_librarys()
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				  	$download_id = $json_array->download_id;
				  	$user_id = $aa['data']->user_id;
					$final_output = array(); $response='failed';
					if(!empty($download_id) && $download_id != 0)
				   	{
					   	$select = $this->common_model->getDataField("download_id","qalame_download_file",array('download_id'=>$download_id));
				   		if(!empty($select))
				   		{	
				   			$response = $this->common_model->deleteData("qalame_download_file",array('download_id'=>$download_id,'user_id'=>$user_id));
			   			}else
				   		{
				   			$response = 'TRUE';
				   		}
				   		if($response=='TRUE')
				   		{ 
				   			$final_output['status'] = 'success';
					   	  	$final_output['message'] = "Successfully deleted";
				   	  	}else
				   	  	{
							$final_output['status'] = 'failed';
						   	$final_output['message'] = constant("some_error_".$json_array->language."");
				   	  	}
				   	}else
				   	{
						$final_output['status'] = 'failed';
		   	  			$final_output['message'] = 'Invalid Request parameter';
			       	}
	   			}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end delete comment

	function Getpost_Detail_ByID()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); //type 1=follow, 2=following
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				  	$user_id = $aa['data']->user_id;
				 	$post_id = $json_array->post_id;
				 	
				  	$key = $this->db->query("SELECT qalame_user_post.post_id,qalame_user_post.preview,qalame_user_post.user_interest,qalame_user_post.user_id,qalame_user_post.title,qalame_user_post.image,qalame_user_post.create_date,qalame_user_post.file_type,qalame_user.user_name,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,location_list.name as location_name FROM `qalame_user_post` INNER JOIN qalame_user ON qalame_user_post.user_id = qalame_user.user_id LEFT JOIN  location_list ON qalame_user.user_location = location_list.location_id where  qalame_user_post.post_id='$post_id' AND qalame_user_post.post_status=1 AND qalame_user.admin_status = 1 AND qalame_user.user_status = 1 ")->row();
				  	if(!empty($key))
					{
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}

							if($key->file_type == 2)
							{
								$url = base_url().'uploads/post/video/'.$key->image;
							}else
							{
								$url = base_url().'uploads/post/image/'.$key->image;
							}
							if($key->file_type != 1)
							{
								$previewurl = base_url().'uploads/post/preview/'.$key->preview;
							}else
							{
								$previewurl = '';
							}
							$key->following_status = 0;
							if($user_id != $key->user_id)
							{
								$followstatus = $this->db->select('request_status')->get_where("qalame_follow_user",array('follower_id'=>$user_id,'following_id'=>$key->user_id))->row();
								if(!empty($followstatus))
								{
									$key->following_status = $followstatus->request_status;
								}
							}
							if($key->location_name==null)
							{
								$key->location_name = '';
							}
							$this->db->where('post_id', $post_id);
							$this->db->where('status', 1);
							$like_count = $this->db->count_all_results('qalame_like_unlike_post');	//following count

							$this->db->where('post_id', $post_id);
							$comment_count = $this->db->count_all_results('qalame_user_post_comment');	//following count

							$like_status = $this->db->select('like_id')->get_where('qalame_like_unlike_post',array('user_id'=>$user_id,'post_id'=>$post_id,'status'=>1))->row();
							if(!empty($like_status))
							{
								$likestatus = 1;
							}else
							{
								$likestatus = 0;							
							}
							unset($key->user_location);
							unset($key->image);
							$key->show_date = strtoupper(date("Y M d",strtotime($key->create_date)));
							$key->user_image = $image;
							$key->post_image = $url;
							$key->preview = $previewurl;
							$key->like_count = $like_count;
							$key->comment_count = $comment_count;
							$key->user_like_status = $likestatus;
							$arr = $key;
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'delete_post';
			   	  		$final_output['message'] = 'Post not available';
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end post by id(Y)

	function user_pending_request()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				  	$user_id = $aa['data']->user_id;
				    $arr = $final_output = array();
				  	$sellist = $this->common_model->getDataField("user_email,user_name,user_id,user_image,user_location,account_mode,qalame_follow_user.create_date,request_status","qalame_follow_user",array('qalame_user.admin_status'=>1,'qalame_user.user_status'=>1,'qalame_follow_user.request_status'=>2,'qalame_follow_user.following_id'=>$user_id),'qalame_follow_user.create_date','DESC',array('qalame_user'=>'qalame_follow_user.follower_id=qalame_user.user_id'));
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$locationame = $this->db->select("name")->get_where("location_list",array('location_id'=>$key->user_location))->row();
							if(!empty($locationame))
							{
								$l_name = $locationame->name;
							}else
							{
								$l_name = '';
							}
							if(!empty($key->user_image))
							{
								$image = base_url().'uploads/user_image/'.$key->user_image;
							}else
							{
								$image = '';
							}
							// $restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->user_id))->row();
							// if(!empty($restatus))
							// {
							// 	$r_status = $restatus->request_status;
							// }else
							// {
							// 	$r_status = 0;
							// }

							$key->location_name = $l_name;
							$key->user_image = $image;
							$key->following_status = 2;
							unset($key->user_location);
							//unset($key->user_id);
							$arr[] = $key;
						} 
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("followlist_".$json_array->language."_2");
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end user list all data (Y)

	function user_notification_loadmore()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				    $user_id = $aa['data']->user_id;
				    $createat = $json_array->create_date;
				    $date = '';
				    if($createat != 0)
				    {
				    	$date = "AND create_date < '$createat'";
				    }
				    $this->db->where('following_id', $user_id);
					$this->db->where('request_status', 2);
					$requestcount = $this->db->count_all_results('qalame_follow_user');	//request count 
				  	//$sellist = $this->common_model->getDataField("user_name,user_id,user_image,user_location","qalame_user",array('admin_status'=>1,'user_status'=>1),'qalame_user.user_id','DESC');
				  	$sellist = $this->db->query("SELECT notification_id,post_id,type,receiver_id,sender_id,create_date FROM qalame_notification WHERE type != 5 AND receiver_id = '$user_id' ".$date." ORDER BY create_date DESC LIMIT 15")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$uname = $image = ''; $object = (object)array();
							if($key->type==4)
							{
			  					$l_name  =''; $r_status = "0"; 
				  				$sellist = $this->db->query("SELECT qalame_user.user_email,qalame_user.user_name,qalame_user.user_id,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,qalame_user.create_date,location_list.name FROM qalame_user LEFT JOIN location_list ON qalame_user.user_location = location_list.location_id WHERE qalame_user.user_id = '".$key->sender_id."'")->row();
								if(!empty($sellist))
								{
									if($sellist->name!=null)
									{
										$l_name = $sellist->name;
									}
									$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->sender_id))->row();
									if(!empty($restatus))
									{
										$r_status = $restatus->request_status;
									}
									if(!empty($sellist->user_image))
									{
										$image = base_url().'uploads/user_image/'.$sellist->user_image;
									}
									$uname = $sellist->user_name;

									$object = array(
											'user_name'=>$uname,
											'user_email'=>$sellist->user_email,
											'user_image'=>$image,
											'following_status'=>$r_status,
											'account_mode'=>$sellist->account_mode,
											'user_id'=>$sellist->user_id,
											'location_name'=>$l_name
										);
								}
							}else
							{
				  				$sellist = $this->db->query("SELECT user_name,user_image FROM qalame_user WHERE user_id = '".$key->sender_id."'")->row();
								if(!empty($sellist))
								{
									$uname = $sellist->user_name;
									if(!empty($sellist->user_image))
									{
										$image = base_url().'uploads/user_image/'.$sellist->user_image;
									}
								}
							}
							$msg = $uname.' '.constant("type_".$key->type);
							
							$arr[] = array(
									'post_id'=>$key->post_id,
									'sender_id'=>$key->sender_id,
									'user_image'=>$image,
									'msg'=>$msg,
									'type'=>$key->type,
									'user_detail'=>$object,							
									'create_date'=>$key->create_date
									);
						}
						
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
						$final_output['request_count'] = $requestcount;

				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("noti_list_".$json_array->language."");
						$final_output['request_count'] = $requestcount;
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end notification list load more(Y)

	function user_notification_pulltorefresh()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
			if(!empty($json_array->language)) 
			{
				if($aa['data']->admin_status ==1) 
				{
				    $arr = $final_output = array();
				    $user_id = $aa['data']->user_id;
				    $createat = $json_array->create_date;
				    $date = '';
				    if($createat != 0)
				    {
				    	$date = "AND create_date >= '$createat'";
				    }
				    $this->db->where('following_id', $user_id);
					$this->db->where('request_status', 2);
					$requestcount = $this->db->count_all_results('qalame_follow_user');	//request count 
				  	//$sellist = $this->common_model->getDataField("user_name,user_id,user_image,user_location","qalame_user",array('admin_status'=>1,'user_status'=>1),'qalame_user.user_id','DESC');
				  	$sellist = $this->db->query("SELECT notification_id,post_id,type,receiver_id,sender_id,create_date FROM qalame_notification WHERE type != 5 AND receiver_id = '$user_id' ".$date." ORDER BY create_date DESC LIMIT 15")->result();
				  	if(!empty($sellist))
					{
						foreach ($sellist as $key) {
							$uname = $image = ''; $object = (object)array();
							if($key->type==4)
							{
			  					$l_name  =''; $r_status = "0"; 
				  				$sellist = $this->db->query("SELECT qalame_user.user_email,qalame_user.user_name,qalame_user.user_id,qalame_user.user_image,qalame_user.user_location,qalame_user.account_mode,qalame_user.create_date,location_list.name FROM qalame_user LEFT JOIN location_list ON qalame_user.user_location = location_list.location_id WHERE qalame_user.user_id = '".$key->sender_id."'")->row();
								if(!empty($sellist))
								{
									if($sellist->name!=null)
									{
										$l_name = $sellist->name;
									}
									$restatus = $this->db->select("request_status")->get_where("qalame_follow_user",array('follower_id'=>$aa['data']->user_id,'following_id'=>$key->sender_id))->row();
									if(!empty($restatus))
									{
										$r_status = $restatus->request_status;
									}
									if(!empty($sellist->user_image))
									{
										$image = base_url().'uploads/user_image/'.$sellist->user_image;
									}
									$uname = $sellist->user_name;

									$object = array(
											'user_name'=>$uname,
											'user_email'=>$sellist->user_email,
											'user_image'=>$image,
											'following_status'=>$r_status,
											'account_mode'=>$sellist->account_mode,
											'user_id'=>$sellist->user_id,
											'location_name'=>$l_name
										);
								}
							}else
							{
				  				$sellist = $this->db->query("SELECT user_name,user_image FROM qalame_user WHERE user_id = '".$key->sender_id."'")->row();
								if(!empty($sellist))
								{
									$uname = $sellist->user_name;
									if(!empty($sellist->user_image))
									{
										$image = base_url().'uploads/user_image/'.$sellist->user_image;
									}
								}
							}
							$msg = $uname.' '.constant("type_".$key->type);
							
							$arr[] = array(
									'post_id'=>$key->post_id,
									'sender_id'=>$key->sender_id,
									'user_image'=>$image,
									'msg'=>$msg,
									'type'=>$key->type,
									'user_detail'=>$object,							
									'create_date'=>$key->create_date
									);
						}
						
						$final_output['status'] = 'success';
						$final_output['message'] = "Successfully";
						$final_output['data'] = $arr;
						$final_output['request_count'] = $requestcount;

				  	}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = constant("noti_list_".$json_array->language."");
						$final_output['request_count'] = $requestcount;
					}
				}else{
					$final_output['status'] = 'failed';
					$final_output['message'] = constant("admin_status_".$json_array->language."");
				}
			}else
			{
				$final_output['status'] = 'failed';
	    		$final_output['message'] = "No Request Parameter Found.";
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end notification list pull to refresh(Y)

	function test_function()
	{
			

		$this->common_model->sendPushNotification('c56NTCiJKbk:APA91bGmrmz2gZoOjN5PvoeWzQbzfqucWI4uKlOhtRmoJ14chfKR7GMDXCtq2F0yQellWETdw4jlShAOP7gwfs0b0HpOyPXSeFnjZdfk-htqB1qLNvlwT7vvinxqh6ZZmPQpSPPGdV_I',array('msg'=>'Hi this is testing notification'));
		exit;
		$url = "<a href=".base_url()."email_template/change_password/".$this->common_model->encryptor_ym('encrypt',1)."/".'ASF123'." target='_blank' title='' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
		$data = array('email'=>'jaipal.solanki@ebabu.co',
                                  'url'=>$url,
                                  'name'=>'Jaipal solanki'
                                );
	    $message = $this->load->view('email_template/template_forgot_pass.php',$data,TRUE);
		print_r($message);exit;
		/*$subject = 'Qalame App: Verification Link';
    	$email_from = 'no-reply@qalame.com';
      	$email = 'yatindra.mohite@ebabu.co';
      	$headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$email_from. '\r\n';            // Mail it
   		@mail($email, $subject,$message,$headers);			*/
	}
	//

	function send_otp()
	{
		$account['user'] = 'wzzicrqb';
		$account['pass'] = 'f8QByuQu';
		$account['from'] = 'SMSALERT';
		$this->load->library('Sms_global',$account);
		
		$this->sms_global->to('919754743271');
		$this->sms_global->message('A message goes in here');
		$this->sms_global->send();

		$id = $this->sms_global->get_sms_id(); // this is the sms id

		$this->sms_global->print_debugger(); // only use this to output the message details on screen for debugging
	}

	function check_authentication()
	{
	    $response = '';
	 	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$final_output['data'] = $check['data'];
				$final_output['status'] = "true";
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			/*if(!isset($headers['secret_key']))
			{
				$json = file_get_contents('php://input');
	    		$json_array = json_decode($json);
				if($json_array->authenticate == 'false')
				{
					return "success";
				}
			}*/
			$final_output['status'] ="false";
			$final_output['message'] = "Unauthorised access";
		}
	    return $final_output;	
	}
	
	function ChechAuth($token)
	{
		$auth = $this->common_model->getDataField('user_id,admin_status','qalame_user',array('user_token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] = $auth[0];
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}
	
}
?>