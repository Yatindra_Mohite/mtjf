<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
			
		/*if($this->check_authentication() != 'success')
        die;*/
	}

	function login()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
    	if($json_array->user_contact!='')
    	{
    		$contact = $json_array->user_contact;
    		
				//$otp = $this->common_model->random_number();
			$otp = '123456';
    		$seleuser = $this->common_model->common_getRow('mtjf_user',array('user_contact'=>$contact));
    		if(!empty($seleuser))
    		{
				$update = $this->common_model->updateData('mtjf_user',array('user_otp'=>md5($otp),'user_status'=>0,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));
				$object = array(
					'user_id'=>(string)$seleuser->user_id,
					);
				$final_output['status'] = 'success';
				$final_output['message'] = 'Successfully login';
				$final_output['data'] = $object;
    		}else
    		{
    			$json_array->user_otp = md5($otp);
    			$json_array->create_date = date('Y-m-d H:i:s');
    			$insert = $this->common_model->common_insert("mtjf_user",$json_array);
    			if($insert!=false)
    			{
    				$object = array(
						'user_id'=>(string)$insert,
					);
    				$final_output['status'] = 'success';
					$final_output['message'] = 'Successfully login';
					$final_output['data'] = $object;
    			}else
    			{
					$final_output['status'] = 'failed';
					$final_output['message'] = some_error;
    			}
    		}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = param_error;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end login + signup(Y)

	function otp_verification()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->mobile_otp) && !empty($json_array->user_id))
	    {
	    	$checkotp = $this->common_model->common_getRow("mtjf_user",array('user_id'=>$json_array->user_id,'user_otp'=>md5($json_array->mobile_otp)));
	    	if(!empty($checkotp))
	    	{	
    			$token = bin2hex(openssl_random_pseudo_bytes(16));
				$token = $token.militime;
	    		$updateotp = $this->common_model->updateData("mtjf_user",array('user_otp'=>'','user_status'=>1,'user_device_type'=>$json_array->user_device_type,'user_device_id'=>$json_array->user_device_id,'user_device_token'=>$json_array->user_device_token,'user_token'=>$token,'update_date'=>date('Y-m-d H:s:i')),array('user_id'=>$json_array->user_id));
	    		if($updateotp!=false)
	    		{
					$updatedevicetoken = $this->common_model->updateData('mtjf_user',array('user_device_token'=>''),array('user_id !='=>$checkotp->user_id,'user_device_id'=>$json_array->user_device_id));
		    			
	    			$image = '';
					if(!empty($checkotp->user_image))
					{
						if (filter_var($checkotp->user_image, FILTER_VALIDATE_URL)) {
						    $image = $checkotp->user_image;
						}else
						{
							$image = base_url().'uploads/user_image/'.$checkotp->user_image;
						}
					}
	    			$object = array(
						'user_id'=>$checkotp->user_id,
						'user_name'=>$checkotp->user_name,
						'user_facebook_id'=>$checkotp->user_facebook_id,
						'user_image'=>$image,
						'user_gender'=>$checkotp->user_gender,
						'user_country_id'=>$checkotp->user_country_id,
						'user_country'=>$checkotp->user_country,
						'user_device_type'=>$json_array->user_device_type,
						'user_device_id'=>$json_array->user_device_id,
						'user_device_token'=>$json_array->user_device_token,
						'user_token'=>$token,
					);
	    			$final_output['status'] = 'success';
					$final_output['message'] = 'OTP has been verified successfully.';
					$final_output['data'] = $object;
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = some_error;
				}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = "Otp does not match.";		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = some_error;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	// End otp verification
	
	function update_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$data['user_name'] = $this->input->post('user_name');
			$data['user_gender'] = $this->input->post('user_gender');
			$facebookid = $this->input->post('user_facebook_id');
			$fb_image = $this->input->post('facebook_image');
			$data['login_type'] = 1;
			if($facebookid!='')
			{
				$data['user_facebook_id'] = $facebookid;
				$data['login_type'] = 2;
			}
			$image = $fb_image;
			if(!isset($_FILES["image"]) || $_FILES["image"]=='')
            {
             	if($fb_image=='')
				{
					$getdata = $this->db->select('user_image')->get_where("mtjf_user",array('user_id'=>$aa['data']->user_id))->row();	
					if($getdata->user_image!=''){
						if (filter_var($getdata->user_image, FILTER_VALIDATE_URL)) {
				    		$image = $getdata->user_image;
						}else
						{
							$image = base_url().'uploads/user_image/'.$getdata->user_image;
						}
					}
				}else
				{
					$data['user_image'] = $fb_image;
				}
            }
            else
            {
                $images=$_FILES["image"]["name"];
    			//$subFileName = explode('.',$_FILES['image']['name']);
				// $ExtFileName = end($subFileName);
			    $images = md5(militime.$images).'.png';
                move_uploaded_file($_FILES["image"]["tmp_name"],"uploads/user_image/".$images);
               	$image = base_url().'uploads/user_image/'.$images;
            	$data['user_image'] = $images;
            }
			$data['update_date'] = date('Y-m-d H:i:s');
			$update_data = $this->common_model->updateData("mtjf_user",$data,array('user_id'=>$aa['data']->user_id));
			if($this->db->affected_rows())
			{
				$data['user_image'] = $image;
				$final_output['status'] = 'success'; 
			 	$final_output['message'] = 'Profile successfully updated.';
				$final_output['data'] = $data;
			}else
			{
				$final_output['status'] = 'failed'; 
			 	$final_output['message'] = some_error;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end update profile (Y)
	
	function view_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
            $user_id = $aa['data']->user_id;
		
	    	$checkotp = $this->db->select('user_name,user_facebook_id,user_image,user_country_id,user_country,user_gender')->get_where("mtjf_user",array('user_id'=>$user_id))->row();
	    	if(!empty($checkotp))
	    	{	
				$image = ''; $contact = array();
				if(!empty($checkotp->user_image))
				{
					if (filter_var($checkotp->user_image, FILTER_VALIDATE_URL)) {
					    $image = $checkotp->user_image;
					}else
					{
						$image = base_url().'uploads/user_image/'.$checkotp->user_image;
					}
				}
				$seluservote = $this->db->query("SELECT like_id,second_user_id,contact_no FROM mtjf_user_like_unlike WHERE user_id = '$user_id'")->result(); //OLD
				//$seluservote = $this->db->query("SELECT mtjf_user_like_unlike.like_id,mtjf_user_like_unlike.second_user_id,mtjf_user_like_unlike.contact_no FROM mtjf_user_like_unlike INNER JOIN mtjf_user_contact_list ON mtjf_user_like_unlike.contact_no = mtjf_user_contact_list.contact_no WHERE mtjf_user_like_unlike.user_id = '$user_id' AND mtjf_user_contact_list.user_id = '$user_id'")->result();
				if(!empty($seluservote))
				{
					foreach ($seluservote as $key) {
						if($key->second_user_id==0)
						{
							$selectuser = $this->db->query("SELECT user_id FROM mtjf_user WHERE CONCAT(user_country_id,'',user_contact) = '".$key->contact_no."'")->row();
				            if($selectuser)
		                    {
		                        $id[] = $selectuser->user_id;
		                    }else
		                    {
		                        $selectuser1 = $this->db->query("SELECT user_id FROM mtjf_user WHERE REPLACE(CONCAT(user_country_id,'',user_contact), '+', '') = '".$key->contact_no."'")->row();
		                        if($selectuser1)
		                        {
		                            $id[] = $selectuser1->user_id;
		                        }else
		                        {
		                            $selectuser2 = $this->db->query("SELECT user_id FROM mtjf_user WHERE user_contact = '".$key->contact_no."'")->row();
		                            if($selectuser2)
		                            {
		                                $id[] = $selectuser2->user_id;
		                   	        }else
		                            {
			                            $selectuser3 = $this->db->query("SELECT user_id FROM mtjf_user WHERE CONCAT('0','',user_contact) = '".$key->contact_no."'")->row();
		                                if($selectuser3)
		                                {
		                                    $id[] = $selectuser3->user_id;
		                   	            }else
		                   	            {
		                   	            	$contact[] = $key->contact_no;
		                   	            }
		                            }
		                        }
		                    }
						}else
						{
							$id[] = $key->second_user_id;
						}
					}
				} 
				if(!empty($id))
				{
					//$lcount = count($id);
					$impid = implode(',',$id);
				}else
				{
					$impid = 0;
					$id = array();
				}
				$matchcount = 0;
				//$selmatch = $this->db->query("SELECT count(user_id) as mcount FROM `mtjf_user_like_unlike` where (second_user_id = '$user_id' OR contact_no = '".$aa['data']->user_country_id.$aa['data']->user_contact."' OR contact_no = '".$aa['data']->user_contact."' OR contact_no = '".'0'.$aa['data']->user_contact."') AND (user_id IN ($impid))")->row();
	            $selmathch = $this->db->query("SELECT count(like_id) as mcount FROM mtjf_user_like_unlike  WHERE ( ( user_id IN ($impid) ) AND ( second_user_id = '$user_id' OR contact_no LIKE '%".$aa['data']->user_country_id.$aa['data']->user_contact."%' OR contact_no LIKE '%".$aa['data']->user_contact."%' OR contact_no LIKE '%".'0'.$aa['data']->user_contact."%') )")->row();
				if(!empty($selmathch))
				{
					$matchcount = $selmathch->mcount;
				}	
				$likcount = count($id) - $matchcount + count($contact);
        		$fan = $this->db->query("SELECT user_id FROM mtjf_user_like_unlike WHERE ( second_user_id = '$user_id' OR contact_no LIKE '%".$aa['data']->user_country_id.$aa['data']->user_contact."%' OR contact_no LIKE '%".$aa['data']->user_contact."%' OR contact_no LIKE '%".'0'.$aa['data']->user_contact."%')")->result();
            	if(!empty($fan))
            	{
					foreach ($fan as $value) {
						$rr[] = $value->user_id;
					}
				}else
            	{
            		$rr = array();
            	}
            	$kk = array_merge(array_diff($id, $rr), array_diff($rr, $id));
                $fcount = count($kk);
	            $object = array(
					'user_name'=>$checkotp->user_name,
					'user_facebook_id'=>$checkotp->user_facebook_id,
					'user_image'=>$image,
					'user_gender'=>$checkotp->user_gender,
					'user_country_id'=>$checkotp->user_country_id,
					'user_country'=>$checkotp->user_country,
					'user_fans'=>(string)$fcount,
					'user_likes'=>(string)$likcount,
					'user_matches'=>(string)$matchcount
					);
	    			$final_output['status'] = 'success';
					$final_output['message'] = 'Successfully get.';
					$final_output['data'] = $object;
			}else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = some_error;
			}
	    }else
		{
			$final_output = $aa;
		}  
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	// End otp verification

	function contact_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
            $user_id = $aa['data']->user_id;
            $myno = $aa['data']->user_contact;
            $json = file_get_contents('php://input');
            if(!empty($json))
            {   
                $data = json_decode($json);
               	$dcount = count($data->friend_data);
               	$date = date('Y-m-d H:i:s');
          	    $userdata = array();
              	$final_output = array();
                if($dcount!=0)
                {
                	$delete = $this->common_model->deleteData("mtjf_user_contact_list",array('user_id'=>$user_id));
					
					for ($i=0; $i < $dcount; $i++) { 
	                	//$newdata[] = "('','" .$user_id. "','','" .$data->friend_data[$i]->contact_no."','" .$data->friend_data[$i]->contact_name."','','".$date."','')";
	                	$newdata[] = '("","' .$user_id. '","","'.$data->friend_data[$i]->contact_no.'","' .$data->friend_data[$i]->contact_name.'","","'.$date.'","")';
	                }
	                $contactdata = implode(',',$newdata);
	              // print_r($contactdata);exit;
	                $insertdata = $this->db->query("INSERT INTO mtjf_user_contact_list VALUES ".$contactdata."");
	                if(!empty($insertdata) && $insertdata===true)
	                {
	                	$listsel = $this->db->query("SELECT contact_id,user_id,contact_no,contact_name,facebook_id as user_facebook_id FROM mtjf_user_contact_list WHERE user_id = '$user_id' ORDER BY contact_name")->result();
			    		//$listsel = $this->common_model->getDataField("user_id,contact_no,contact_name,facebook_id as user_facebook_id","mtjf_user_contact_list",array('user_id'=>$user_id));
		            	if(!empty($listsel))
		            	{
		            	 	foreach ($listsel as $values)
			                { 
			                    $contactno =$values->contact_no;
			                    $selectuser = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE CONCAT(user_country_id,'',user_contact) = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
			                    if($selectuser)
			                    {
			                        $usermobile = $selectuser->user_country_id.$selectuser->user_contact;
			                        $id = $selectuser->user_image;
			                   		$uid = $selectuser->user_id;
			                    }else
			                    {
			                        $selectuser1 = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE REPLACE(CONCAT(user_country_id,'',user_contact), '+', '') = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
			                        if($selectuser1)
			                        {
			                        	$usermobile = $selectuser1->user_country_id.$selectuser1->user_contact;
			                            $id = $selectuser1->user_image;
			                   			$uid = $selectuser1->user_id;
			                        }else
			                        {
			                            $selectuser2 = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE user_contact = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
			                            if($selectuser2)
			                            {
			                        		$usermobile = $selectuser2->user_country_id.$selectuser2->user_contact;
			                                $id = $selectuser2->user_image;
			                   				$uid = $selectuser2->user_id;
			                            }else
			                            {
			                                $selectuser3 = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE CONCAT('0','',user_contact) = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
			                                if($selectuser3)
			                                {
			                        			$usermobile = $selectuser3->user_country_id.$selectuser3->user_contact;
			                                    $id = $selectuser3->user_image;
			                   					$uid = $selectuser3->user_id;
			                                }else
			                                {
			                                	$id = '';
			                                	$uid = 0;
			                                	$usermobile = '';
			                                }
			                            }
			                        }
			                    }
			                    $fstatus = 0; $hint= '';
			                    $mathch = $this->db->query("SELECT a.like_id,b.like_id as lid FROM mtjf_user_like_unlike as a INNER JOIN mtjf_user_like_unlike as b WHERE ( ( a.user_id = '$user_id' ) AND (a.second_user_id = '$uid' OR a.contact_no LIKE '%$contactno%') ) AND ( ( b.user_id = '$uid' ) AND ( b.second_user_id = '$user_id' OR b.contact_no LIKE '%".$aa['data']->user_country_id."$myno%' OR b.contact_no LIKE '%$myno%' OR b.contact_no LIKE '%".'0'."$myno%') )")->row();
			                    if(!empty($mathch))
			                    {
			                    	$fstatus = 1; //match
			                    }else
			                    {
			                    	$like = $this->db->query("SELECT like_id,like_hint FROM mtjf_user_like_unlike WHERE (user_id = '$user_id' AND contact_no = '$contactno')")->row();
			                    	if(!empty($like))
			                    	{
			                    		$fstatus = 2; //like
			                    		$hint = $like->like_hint;
			                    	}/*else
			                    	{
			                    		$fan = $this->db->query("SELECT like_id FROM mtjf_user_like_unlike WHERE (user_id = '$uid') AND ( second_user_id = '$user_id' OR contact_no LIKE '%".$aa['data']->user_country_id."$myno%' OR contact_no LIKE '%$myno%' OR contact_no LIKE '%".'0'."$myno%')")->row();
				                    	if(!empty($fan))
				                    	{
				                    		$fstatus = 3; //fan
				                    	}
			                    	}	*/	
			                    }
			                    
			                    if($id!='' )
								{
									if (filter_var($id, FILTER_VALIDATE_URL)) {
							    		$values->user_image = $id;
									}else
									{
										$values->user_image = base_url().'uploads/user_image/'.$id;
									}
								}else
								{
									$values->user_image = '';
								}
								$values->full_contact_no = $usermobile;
								$values->friend_status = $fstatus; 
								$values->like_hint = $hint; 
								$values->userid = (string)$uid;
								$arr[] = $values;
			                }
			               
		            	}
		            	if(!empty($arr))
		                {
		                	$final_output["status"] = "success";
	                		$final_output["message"] = "Contact list successfully added.";	
	                		$final_output["data"] = $arr;	
		                }else
		                {
		                	$final_output["status"] = "failed";
	                		$final_output["message"] = some_error;	
		                }
	                }
	                else
	                {
	                    $final_output["status"] = "failed";
	                    $final_output["message"] = some_error;
	        	    }
            	}else
            	{
            		$final_output["status"] = "failed";
                	$final_output["message"] = "No required parameter";		
            	}
            }
            else
            {
                $final_output["status"] = "failed";
                $final_output["message"] = "No required parameter";
            } 
		}else
		{
			$final_output = $aa;
		}  
		header("content-type: application/json");
		echo json_encode($final_output);
	}
	//End Add contact

	/*public function facebook_contact_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
            $user_id = $aa['data']->user_id;
            $user_facebook_id = $aa['data']->user_facebook_id;
            $json = file_get_contents('php://input');
            if(!empty($json))
            {   
                $data = json_decode($json);
               	$dcount = count($data->friend_data);
               	$date = date('Y-m-d H:i:s');
          	    $userdata = array();
              	$final_output = array();
                if($dcount!=0)
                {
                	$delete = $this->common_model->deleteData("mtjf_user_contact_list",array('user_facebook_id'=>$user_facebook_id,));
					
					for ($i=0; $i < $dcount; $i++) { 
	                	
	                	$newdata[] = "('','" .$user_id. "','".$user_facebook_id."','" .$data->friend_data[$i]->contact_no."','" .$data->friend_data[$i]->contact_name."','".$data->friend_data[$i]->contact_fb_id."','".$date."','')";
	                }
	                $contactdata = implode(',',$newdata);
	              // print_r($contactdata);exit;
	                $insertdata = $this->db->query("INSERT INTO mtjf_user_contact_list VALUES ".$contactdata."");
	                if(!empty($insertdata) && $insertdata===true)
	                {
	                    $final_output["status"] = "success";
                        $final_output["message"] = "Contact list successfully added.";
                        //$final_output["friend_list"] = $arr;
	                }
	                else
	                {
	                    $final_output["status"] = "failed";
	                    $final_output["message"] = some_error;
	        	    }
            	}else
            	{
            		$final_output["status"] = "failed";
                	$final_output["message"] = "No required parameter";		
            	}
            }
            else
            {
                $final_output["status"] = "failed";
                $final_output["message"] = "No required parameter";
            } 
		}else
		{
			$final_output = $aa;
		}  
		header("content-type: application/json");
		echo json_encode($final_output);
	}*/
	//End Add facebook contact

	function Get_contact_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
            	$user_id = $aa['data']->user_id;
            	$myno = $aa['data']->user_contact;
                $page_no = $this->input->post('page_no');
                $arr = array(); $id = $uid = '';
              	$final_output = array();
            	$limitt = '';
		       	$per_set = 200;
		       	$from  = ($page_no-1)*$per_set;
		       	$limitt = "limit ".$from.",".$per_set."";
		       	$listsel = $this->db->query("SELECT contact_id,user_id,contact_no,contact_name,facebook_id as user_facebook_id FROM mtjf_user_contact_list WHERE user_id = '$user_id' ORDER BY contact_name $limitt")->result();
			    //$listsel = $this->common_model->getDataField("user_id,contact_no,contact_name,facebook_id as user_facebook_id","mtjf_user_contact_list",array('user_id'=>$user_id));
            	if(!empty($listsel))
            	{
            	 	foreach ($listsel as $values)
	                { 
	                    $contactno =$values->contact_no;
	                    $selectuser = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE CONCAT(user_country_id,'',user_contact) = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
	                    if($selectuser)
	                    {
	                        $usermobile = $selectuser->user_country_id.$selectuser->user_contact;
	                        $id = $selectuser->user_image;
	                   		$uid = $selectuser->user_id;
	                    }else
	                    {
	                        $selectuser1 = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE REPLACE(CONCAT(user_country_id,'',user_contact), '+', '') = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
	                        if($selectuser1)
	                        {
	                        	$usermobile = $selectuser1->user_country_id.$selectuser1->user_contact;
	                            $id = $selectuser1->user_image;
	                   			$uid = $selectuser1->user_id;
	                        }else
	                        {
	                            $selectuser2 = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE user_contact = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
	                            if($selectuser2)
	                            {
	                        		$usermobile = $selectuser2->user_country_id.$selectuser2->user_contact;
	                                $id = $selectuser2->user_image;
	                   				$uid = $selectuser2->user_id;
	                            }else
	                            {
	                                $selectuser3 = $this->db->query("SELECT user_country_id,user_contact,user_image,user_id FROM mtjf_user WHERE CONCAT('0','',user_contact) = '".$contactno."' AND user_id !='".$values->user_id."'")->row();
	                                if($selectuser3)
	                                {
	                        			$usermobile = $selectuser3->user_country_id.$selectuser3->user_contact;
	                                    $id = $selectuser3->user_image;
	                   					$uid = $selectuser3->user_id;
	                                }else
	                                {
	                                	$id = '';
	                                	$uid = 0;
	                                	$usermobile = '';
	                                }
	                            }
	                        }
	                    }
	                    $fstatus = 0; $hint = '';
	                    $mathch = $this->db->query("SELECT a.like_id,b.like_id as lid FROM mtjf_user_like_unlike as a INNER JOIN mtjf_user_like_unlike as b WHERE ( ( a.user_id = '$user_id' ) AND (a.second_user_id = '$uid' OR a.contact_no LIKE '%$contactno%') ) AND ( ( b.user_id = '$uid' ) AND ( b.second_user_id = '$user_id' OR b.contact_no LIKE '%".$aa['data']->user_country_id."$myno%' OR b.contact_no LIKE '%$myno%' OR b.contact_no LIKE '%".'0'."$myno%') )")->row();
	                    if(!empty($mathch))
	                    {
	                    	$fstatus = 1; //match
	                    }else
	                    {
	                    	$like = $this->db->query("SELECT like_id,like_hint FROM mtjf_user_like_unlike WHERE (user_id = '$user_id' AND contact_no = '$contactno')")->row();
	                    	if(!empty($like))
	                    	{
	                    		$fstatus = 2; //like
			                	$hint = $like->like_hint;
	                    	}/*else
	                    	{
	                    		$fan = $this->db->query("SELECT like_id FROM mtjf_user_like_unlike WHERE (user_id = '$uid') AND ( second_user_id = '$user_id' OR contact_no LIKE '%".$aa['data']->user_country_id."$myno%' OR contact_no LIKE '%$myno%' OR contact_no LIKE '%".'0'."$myno%')")->row();
		                    	if(!empty($fan))
		                    	{
		                    		$fstatus = 3; //fan
		                    	}
	                    	}	*/	
	                    }
	                    
	                    if($id!='' )
						{
							if (filter_var($id, FILTER_VALIDATE_URL)) {
					    		$values->user_image = $id;
							}else
							{
								$values->user_image = base_url().'uploads/user_image/'.$id;
							}
						}else
						{
							$values->user_image = '';
						}
						$values->full_contact_no = $usermobile;
						$values->friend_status = $fstatus; 
						$values->like_hint = $hint; 
						$values->userid = (string)$uid;
						$arr[] = $values;
	                }
	                if(!empty($arr))
	                {
	                	$final_output["status"] = "success";
                		$final_output["message"] = "Contact List";	
                		$final_output["data"] = $arr;	
	                }else
	                {
	                	$final_output["status"] = "failed";
                		$final_output["message"] = "Empty contact list";	
	                }
            	}else
            	{
            		$final_output["status"] = "failed";
                	$final_output["message"] = "Empty contact list";		
            	}
     	}else
		{
			$final_output = $aa;
		}  
		header("content-type: application/json");
		echo json_encode($final_output);
	}
	//End Get Contact list

	function Like_unlike_user()
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
		  	$userid = $json_array->userid;
		  	$contact_no = $json_array->contact_no;
		  	$like_hint = $json_array->like_hint;
		  	$like_status = $json_array->status; //1=like, 0=unlike
		  	$user_id = $aa['data']->user_id;
			$final_output = array();
			if($userid == 0)
			{
				$check = "AND contact_no = '$contact_no'";
			}else
			{
				$check = "AND second_user_id = '$userid'";
			}
			$response = ''; $likes = false;
			$coinbal = $this->db->select('user_wallet')->get_where('mtjf_user',array('user_id'=>$user_id))->row();
			$uname = '';
			$name = $this->db->query("SELECT user_name FROM mtjf_user WHERE CONCAT(user_country_id,'',user_contact) = '$contact_no' OR user_contact = '$contact_no'")->row();
			if(!empty($name))
			{
				$uname = $name->user_name;
			}else
			{
				$name = $this->db->query("SELECT contact_name FROM mtjf_user_contact_list WHERE contact_no = '$contact_no'")->row();
				if(!empty($name))
				{
					$uname = $name->contact_name;
				}
			}

			$seluservote = $this->db->query("SELECT like_id FROM mtjf_user_like_unlike WHERE user_id = '$user_id' AND contact_no LIKE '%$contact_no%' ")->row();
			if(!empty($seluservote))
			{
				if($like_status != 1)
				{
					if($coinbal->user_wallet!=0 && $coinbal->user_wallet!=null)
					{
						$unlike = $this->common_model->deleteData("mtjf_user_like_unlike",array('like_id'=>$seluservote->like_id));
						if($unlike==TRUE)
						{
							$msg = 'Undo liking '.$uname;
							$new_wallet = $coinbal->user_wallet-1;
							$updatecoin = $this->db->query("UPDATE mtjf_user SET user_wallet = '$new_wallet' WHERE user_id = '$user_id'");
							$insertcoin = $this->db->insert("coin_history",array('user_id'=>$user_id,'actions'=>'deduct','msg'=>$msg,'coin'=>1,'coin_balance'=>$new_wallet,'create_date'=>date('Y-m-d H:i:s')));
							$response = 'true';
			    			$status = 0;
						}else
			   	 		{
							$response = 'failed';
				  		}
					}else
					{
						$response = 'nocoin';
					}	
		   		}else{
					$response = 'false';
		    		$seluser = $this->db->query("SELECT user_id FROM mtjf_user WHERE CONCAT(user_country_id,'',user_contact) LIKE  '%".$contact_no."%' OR user_contact LIKE '%".$contact_no."%'")->row();
					if(!empty($seluser)){ $uid = $seluser->user_id; }else{ $uid = 0; }		
					$matchcheck = $this->db->query("SELECT like_id FROM mtjf_user_like_unlike WHERE (user_id = '$uid') AND (second_user_id = '$user_id' OR contact_no = '".$aa['data']->user_contact."' OR contact_no = '".$aa['data']->user_country_id.$aa['data']->user_contact."')")->row();
					if(!empty($matchcheck))
					{
						$status = 1;
					}else
					{
						$status = 2;
					}
		    	}	
			}else
			{
				if($like_status== 1)
				{
					if($like_hint == '')
					{
						$likes = $this->common_model->common_insert("mtjf_user_like_unlike",array('user_id'=>$user_id,'second_user_id'=>$userid,'contact_no'=>$contact_no,'like_hint'=>$like_hint,'create_date'=>date('Y-m-d H:i:s')));
					}else
					{	
						if($coinbal->user_wallet!=0 && $coinbal->user_wallet!=null)
						{
							$likes = $this->common_model->common_insert("mtjf_user_like_unlike",array('user_id'=>$user_id,'second_user_id'=>$userid,'contact_no'=>$contact_no,'like_hint'=>$like_hint,'create_date'=>date('Y-m-d H:i:s')));
						}else
						{
							$response = 'nocoin';
						}
					}
					if($likes==TRUE)
					{
						if($like_hint!='')
						{
							$msg = 'Liked '.$uname;
							$new_wallet = $coinbal->user_wallet-1;
							$updatecoin = $this->db->query("UPDATE mtjf_user SET user_wallet = '$new_wallet' WHERE user_id = '$user_id'");
							$insertcoin = $this->db->insert("coin_history",array('user_id'=>$user_id,'actions'=>'deduct','msg'=>$msg,'coin'=>1,'coin_balance'=>$new_wallet,'create_date'=>date('Y-m-d H:i:s')));
						}
						$matchcheck = $this->db->query("SELECT like_id FROM mtjf_user_like_unlike WHERE (user_id = '$userid') AND ('second_user_id' = '$user_id' OR contact_no LIKE '%".$aa['data']->user_country_id.$aa['data']->user_contact."%' OR contact_no LIKE '%".$aa['data']->user_contact."%')")->row();
						if(!empty($matchcheck))
						{
							// $gettoken = $this->db->select('device_type,device_token')->get_where("mtjf_user",array('user_id'=>$userid))->row();
							// $msg = array('msg'=>"You have new match",'image'=>'','contact_name'=>'','contact_no'=>'');
							// sendPushNotification($gettoken->device_token,$msg);
							$status = 1;
						}else
						{
							$status = 2;
						}
						//send notification
						$response = 'true';
					}elseif($response!='nocoin')
		   	 		{
						$response = 'failed';
			  		}
				}else{
					$response = 'false';
		    		$status = 0;
		        }	
			}
				if($response=='true')
				{
					if($like_status == 1){ $msg = 'Successfully Liked'; }else{ $msg = 'Successfully Unliked'; }
					$final_output['status'] = 'success';
	   		  		$final_output['message'] = $msg;
	   	  			$final_output['data'] = array('friend_status'=>$status);
				}
				elseif($response=='nocoin')
				{
					$final_output['status'] = 'failed';
		  			$final_output['message'] = "Insufficient Coins";
				}
				elseif($response=='false')
				{
					if($like_status == 1){ $msg = 'Already Liked'; }else{ $msg = 'Already Unliked'; }
					$final_output['status'] = 'success';
	   	  			$final_output['message'] = $msg;
	   	  			$final_output['data'] = array('friend_status'=>$status);
				}
				else
				{
		 			$final_output['status'] = 'failed';
		  			$final_output['message'] = some_error;
				}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//end like unlike post

	function match_like_detail()  
	{	
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json); 
		  	$user_id = $aa['data']->user_id;
			$type = $json_array->type; //1 = match, 2 = like
			$final_output = array();
			$id= $arr = $seldetail = array();
			$seluservote = $this->db->query("SELECT like_id,second_user_id,contact_no FROM mtjf_user_like_unlike WHERE user_id = '$user_id'")->result();
			if(!empty($seluservote))
			{
				foreach ($seluservote as $key) {
					if($key->second_user_id==0)
					{
						$selectuser = $this->db->query("SELECT user_id FROM mtjf_user WHERE CONCAT(user_country_id,'',user_contact) = '".$key->contact_no."'")->row();
	                    if($selectuser)
	                    {
	                        $id[] = $selectuser->user_id;
	                    }else
	                    {
	                        $selectuser1 = $this->db->query("SELECT user_id FROM mtjf_user WHERE REPLACE(CONCAT(user_country_id,'',user_contact), '+', '') = '".$key->contact_no."'")->row();
	                        if($selectuser1)
	                        {
	                            $id[] = $selectuser1->user_id;
	                        }else
	                        {
	                            $selectuser2 = $this->db->query("SELECT user_id FROM mtjf_user WHERE user_contact = '".$key->contact_no."'")->row();
	                            if($selectuser2)
	                            {
	                                $id[] = $selectuser2->user_id;
	                   	        }else
	                            {
		                            $selectuser3 = $this->db->query("SELECT user_id FROM mtjf_user WHERE CONCAT('0','',user_contact) = '".$key->contact_no."'")->row();
	                                if($selectuser3)
	                                {
	                                    $id[] = $selectuser3->user_id;
	                   	            }else
	                   	            {
	                   	            	$contact[] = $key->contact_no;
	                   	            }
	                            }
	                        }
	                    }
					}else
					{
						$id[] = $key->second_user_id;
					}
				} 
				if(!empty($id))
				{
					//$lcount = count($id);
					$impid = implode(',',$id);
				}else
				{
					$impid = 0;
				}

					$uid =  0; $mat =array(); //match condition
					$selmatch = $this->db->query("SELECT user_id FROM `mtjf_user_like_unlike` where (second_user_id = '$user_id' OR contact_no = '".$aa['data']->user_country_id.$aa['data']->user_contact."' OR contact_no = '".$aa['data']->user_contact."' OR contact_no = '".'0'.$aa['data']->user_contact."') AND (user_id IN ($impid))")->result();
					if(!empty($selmatch))
					{
						foreach ($selmatch as $value) {
							$mat[] = $value->user_id; 
						}
						$uid = implode(',',$mat);
					}	
					if($type==1)
					{
						$seldetail = $this->db->query("SELECT user_name,user_image,user_country_id,user_contact FROM `mtjf_user` where user_id IN ($uid)")->result();
					}else
					{
						//Like Condition
		            	$kk = array_merge(array_diff($id, $mat), array_diff($mat, $id));
						$cc = implode(',', $kk);
						if($cc==''){ $cc = 0; }
						$reguserdetail = $this->db->query("SELECT user_name,user_image,user_country_id,user_contact FROM `mtjf_user` where user_id IN ($cc)")->result(); //register user
						if(!empty($reguserdetail))
						{
							foreach ($reguserdetail as $kevalue) {
								$seldetail[]	 = (object)array('contact_name'=>$kevalue->user_name,'user_image'=>$kevalue->user_image,'user_country_id'=>$kevalue->user_country_id,'contact_no'=>$kevalue->user_contact);	
							}
						}

						if(!empty($contact))
						{
							$imcont = implode(',', $contact);
						}else
						{
							$imcont = 0;
						}
						$nonreguserdetail = $this->db->query("SELECT contact_name,contact_no FROM `mtjf_user_contact_list` where user_id = '$user_id' AND contact_no IN ($imcont)")->result(); //non register user
						if(!empty($nonreguserdetail))
						{
							foreach ($nonreguserdetail as $kevalue2) {
								$seldetail[] = (object)array('contact_name'=>$kevalue2->contact_name,'user_image'=>'','contact_no'=>$kevalue2->contact_no);	
							}
						}
					}
				if(!empty($seldetail))
				{
					foreach ($seldetail as $key) {
						if(isset($key->user_image) && !empty($key->user_image))
						{
							if (filter_var($key->user_image, FILTER_VALIDATE_URL)) {
				    			$key->user_image = $key->user_image;
							}else
							{
								$key->user_image = base_url().'uploads/user_image/'.$key->user_image;
							}
						}else
						{
							$key->user_image ='';
						}
						$usermobile = '';
						if(!isset($key->contact_no))
						{
							$key->contact_no = $key->user_contact;
							$key->contact_name = $key->user_name;
							$usermobile = $key->user_country_id.$key->user_contact;
							unset($key->user_contact);
							unset($key->user_name);
						}
						$key->friend_status = (int)$type;
						$key->full_contact_no = $usermobile;
						$arr[] = $key;
					}
				}
			}
			if(!empty($arr))
			{
				$final_output['status'] = 'success';
   		  		$final_output['message'] = 'Successfully get';
   	  			$final_output['data'] = $arr;
			}
			else
			{
				$final_output['status'] = 'failed';
   	  			$final_output['message'] = "List not available";
   	  		}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output);
	}

	function check_authentication()
	{
	    $response = '';
	 	$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$final_output['data'] = $check['data'];
				$final_output['status'] = "true";
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			/*if(!isset($headers['secret_key']))
			{
				$json = file_get_contents('php://input');
	    		$json_array = json_decode($json);
				if($json_array->authenticate == 'false')
				{
					return "success";
				}
			}*/
			$final_output['status'] ="false";
			$final_output['message'] = "Unauthorised access";
		}
	    return $final_output;	
	}
	
	function ChechAuth($token)
	{
		$auth = $this->common_model->getDataField('user_id,user_facebook_id,user_country_id,user_contact','mtjf_user',array('user_token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] = $auth[0];
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}
	
}
?>